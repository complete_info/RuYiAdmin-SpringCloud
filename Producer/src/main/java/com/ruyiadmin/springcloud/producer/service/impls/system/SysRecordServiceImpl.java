package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysRecord;
import com.ruyiadmin.springcloud.producer.repository.system.ISysRecordRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysRecordService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统记录表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-09-04
 */
@Service
public class SysRecordServiceImpl extends ServiceImpl<ISysRecordRepository, SysRecord> implements ISysRecordService {

}
