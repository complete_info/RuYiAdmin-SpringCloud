package com.ruyiadmin.springcloud.producer.common.utils.core;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileNameUtil;
import com.ruyiadmin.springcloud.producer.common.beans.system.DirectoryConfig;
import com.ruyiadmin.springcloud.producer.common.classes.system.FileInfo;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * Aes加解密工具类
 *
 * @author RuYiAdmin
 * @since 2022-08-03
 */
@Component
public class RuYiFileUtil {

    @Resource
    private DirectoryConfig directoryConfig;
    public static RuYiFileUtil fileUtil;

    //region PostConstruct

    @PostConstruct
    public void init() {
        fileUtil = this;
        fileUtil.directoryConfig = this.directoryConfig;
    }

    //endregion

    //region 获取文件信息

    /**
     * 获取文件信息
     *
     * @param file 上传文件
     * @return FileInfo 文件信息
     */
    public static FileInfo getFileInfo(MultipartFile file) {
        FileInfo fileInfo = new FileInfo();

        long size = file.getSize();
        if (size < 1024) {
            fileInfo.setSize(size);
            fileInfo.setRemark("B");
        } else {
            if (size / 1024 < 1024) {
                BigDecimal bg = new BigDecimal(size / 1024);
                double value = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                fileInfo.setSize(value);
                fileInfo.setRemark("KB");
            } else if (size / (1024 * 1024) < 1024) {
                BigDecimal bg = new BigDecimal(size / (1024 * 1024));
                double value = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                fileInfo.setSize(value);
                fileInfo.setRemark("MB");
            } else {
                BigDecimal bg = new BigDecimal(size / (1024 * 1024 * 1024));
                double value = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                fileInfo.setSize(value);
                fileInfo.setRemark("GB");
            }
        }

        return fileInfo;
    }

    //endregion

    //region 保存业务附件

    /**
     * 保存业务附件
     *
     * @param file 上传文件
     * @param id   文件编号
     * @return 文件路径
     */
    public static String saveBusinessAttachment(MultipartFile file, String id) throws IOException {
        //获取文件拓展名
        String extension = Objects.requireNonNull(file.getOriginalFilename()).substring(
                file.getOriginalFilename().lastIndexOf("."));
        String fileName = String.join("", id, extension);
        String path = fileUtil.directoryConfig.getBusinessAttachmentPath();
        String filePath = String.join("", path, "/", fileName);
        file.transferTo(Paths.get(filePath));
        return filePath;
    }

    //endregion

    //region 获取文件夹大小

    /**
     * 获取文件夹大小
     *
     * @param path 文件夹路径
     */
    public static double getDirectorySizeByMB(String path) {
        long size = FileUtils.sizeOfDirectory(new File(path));
        BigDecimal bg = new BigDecimal(size / (1024 * 1024));
        return bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    //endregion

    //region 保存上传文件

    /**
     * 保存上传文件
     *
     * @param file 上传文件
     * @param id   文件编号
     * @return 文件路径
     */
    public static String saveMultipartFile(MultipartFile file, String id) throws IOException {
        //获取文件拓展名
        String extension = Objects.requireNonNull(file.getOriginalFilename())
                .substring(file.getOriginalFilename().lastIndexOf("."));
        String fileName = String.join("", id, extension);
        String path = fileUtil.directoryConfig.getTempPath();
        String filePath = String.join("", path, "/", fileName);
        InputStream inputStream = null;
        FileOutputStream fileOutStream = null;
        try {
            inputStream = file.getInputStream();
            fileOutStream = new FileOutputStream(filePath);
            IOUtils.copy(inputStream, fileOutStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fileOutStream.flush();
            if (inputStream != null) {
                inputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return filePath;
    }

    //endregion

    //region 拷贝源文件

    /**
     * 拷贝源文件
     *
     * @param path 源文件路径
     * @param id   文件名称
     * @return 新文件路径
     */
    public static String copyFile(String path, String id) {
        String fileName = String.join("", id, ".", FileNameUtil.extName(path));
        String tempPath = fileUtil.directoryConfig.getTempPath();
        String filePath = String.join("", tempPath, "/", fileName);
        FileUtil.copyFile(path, filePath);
        return filePath;
    }

    //endregion

}
