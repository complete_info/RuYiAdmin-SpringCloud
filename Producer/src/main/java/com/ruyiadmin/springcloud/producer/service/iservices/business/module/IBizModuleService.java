package com.ruyiadmin.springcloud.producer.service.iservices.business.module;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.entity.business.module.BizModule;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 模块表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface IBizModuleService extends IService<BizModule> {
    /**
     * 查询无权限模块列表
     *
     * @param userId 用户编号
     * @return QueryResult
     */
    QueryResult<BizModule> queryUserNonModules(String userId);
}
