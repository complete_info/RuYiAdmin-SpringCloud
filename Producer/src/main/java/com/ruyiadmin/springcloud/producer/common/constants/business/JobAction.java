package com.ruyiadmin.springcloud.producer.common.constants.business;

/**
 * <p>
 * RuYiAdmin 作业操作常量
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-04
 */
public class JobAction {
    public static final String Delete = "Delete";
    public static final String Start = "Start";
    public static final String Pause = "Pause";
    public static final String Resume = "Resume";
}
