package com.ruyiadmin.springcloud.producer.common.beans.system;

import com.ruyiadmin.springcloud.producer.common.factories.RuYiAdminYamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 代码生成器配置类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-19
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "codegeneratorconfig")
@PropertySource(value = {"classpath:appSettings.yaml"}, factory = RuYiAdminYamlPropertySourceFactory.class)
@Data
public class CodeGeneratorConfig {
    //是否启用
    private boolean IsEnabled;
    //忽略字段
    private String FieldsIgnoreCase;
}
