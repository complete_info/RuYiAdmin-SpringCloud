package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.alibaba.fastjson.JSON;
import com.ruyiadmin.springcloud.producer.common.beans.system.SystemCacheConfig;
import com.ruyiadmin.springcloud.producer.common.components.core.RuYiRedisComponent;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysCodeTableDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysCodeTable;
import com.ruyiadmin.springcloud.producer.repository.system.ISysCodeTableRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysCodeTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SysCodeTableServiceImpl extends ServiceImpl<ISysCodeTableRepository, SysCodeTable> implements ISysCodeTableService {

    //region 实现类私有属性

    private final RuYiRedisComponent redisUtils;
    private final SystemCacheConfig systemCacheConfig;
    private final ModelMapper modelMapper;

    //endregion

    //region 加载数据字典缓存

    /**
     * 加载数据字典缓存
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void loadSysCodeTableCache() {
        List<SysCodeTableDTO> codeTables = new ArrayList<>();
        List<SysCodeTable> list = this.list();
        for (SysCodeTable codeTable : list) {
            codeTables.add(this.modelMapper.map(codeTable, SysCodeTableDTO.class));
        }
        this.redisUtils.set(systemCacheConfig.getCodeTableCacheName(), JSON.toJSONString(codeTables));
        log.info("RuYiAdmin sys code tables cache loaded");
    }

    //endregion

    //region 清理数据字典缓存

    /**
     * 清理数据字典缓存
     */
    @Override
    public void clearSysCodeTableCache() {
        this.redisUtils.del(systemCacheConfig.getCodeTableCacheName());
        log.info("RuYiAdmin sys code tables cache cleared");
    }

    //endregion

    //region 获取字典树形结构

    /**
     * 获取字典树形结构
     *
     * @return QueryResult
     */
    @Override
    public QueryResult<SysCodeTableDTO> getCodeTreeNodes() {
        Object value = this.redisUtils.get(systemCacheConfig.getCodeTableCacheName());
        List<SysCodeTableDTO> codeTables = JSON.parseArray(value.toString(), SysCodeTableDTO.class);

        List<SysCodeTableDTO> parentCodeTables = codeTables.stream().
                filter(t -> StringUtils.isEmpty(t.getParentId())).
                sorted(Comparator.comparing(SysCodeTableDTO::getSerialNumber)).
                collect(Collectors.toList());

        for (SysCodeTableDTO codeTable : parentCodeTables) {
            //递归子节点
            this.initNodeChildren(codeTable, codeTables);
        }

        List<SysCodeTableDTO> result = new ArrayList<>(parentCodeTables);

        return QueryResult.success(parentCodeTables.size(), result);
    }

    //endregion

    //region 实现类私有方法

    /**
     * 递归子节点
     *
     * @param root       根节点
     * @param codeTables 字典列表
     */
    private void initNodeChildren(SysCodeTableDTO root, List<SysCodeTableDTO> codeTables) {
        List<SysCodeTableDTO> list = codeTables.stream().
                filter(t -> !StringUtils.isEmpty(t.getParentId())).
                filter(t -> t.getParentId().equals(root.getId())).
                collect(Collectors.toList());
        if (list.size() > 0) {
            root.setChildren(new ArrayList<>());
            root.getChildren().addAll(list.stream().
                    sorted(Comparator.comparing(SysCodeTableDTO::getSerialNumber))
                    .collect(Collectors.toList()));
            for (SysCodeTableDTO item : list) {
                this.initNodeChildren(item, codeTables);
            }
        }
    }

    //endregion
}
