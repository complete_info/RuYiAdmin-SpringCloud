package com.ruyiadmin.springcloud.producer.common.classes.system;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 系统验证码类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-16
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RuYiAdminCaptcha {
    //编号
    private String id;
    //验证码
    private String captchaPicture;
    //计算结果
    private String result;
    //表达式
    private String expression;
}
