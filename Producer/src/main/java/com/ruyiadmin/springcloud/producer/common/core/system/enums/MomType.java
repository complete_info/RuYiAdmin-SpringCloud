package com.ruyiadmin.springcloud.producer.common.core.system.enums;

/**
 * <p>
 * 消息中间件类型枚举
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-17
 */
public enum MomType {
    ActiveMQ,
    RabbitMQ
}
