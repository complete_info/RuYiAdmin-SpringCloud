package com.ruyiadmin.springcloud.producer.common.core.system.entities;

import com.ruyiadmin.springcloud.producer.common.core.system.enums.MessageLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 广播消息模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-22
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BroadcastMessage {
    //标题
    private String Title;
    //内容
    private String Message;
    //消息级别
    private MessageLevel MessageLevel;
}
