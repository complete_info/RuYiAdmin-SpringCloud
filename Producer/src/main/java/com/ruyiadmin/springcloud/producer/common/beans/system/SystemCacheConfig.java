package com.ruyiadmin.springcloud.producer.common.beans.system;

import com.ruyiadmin.springcloud.producer.common.factories.RuYiAdminYamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 系统缓存配置类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-14
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "systemcacheconfig")
@PropertySource(value = {"classpath:appSettings.yaml"}, factory = RuYiAdminYamlPropertySourceFactory.class)
@Data
public class SystemCacheConfig {
    //机构缓存名称
    private String OrgCacheName;
    //用户缓存名称
    private String UserCacheName;
    //菜单缓存名称
    private String MenuCacheName;
    //菜单与多语缓存名称
    private String MenuAndLanguageCacheName;
    //角色缓存名称
    private String RoleCacheName;
    //角色菜单缓存名称
    private String RoleAndMenuCacheName;
    //角色机构缓存名称
    private String RoleAndOrgCacheName;
    //角色用户缓存名称
    private String RoleAndUserCacheName;
    //数据字典缓存名称
    private String CodeTableCacheName;
    //多语缓存名称
    private String LanguageCacheName;
    //计划业务缓存名称
    private String ScheduleJobCacheName;
    //行政区域缓存名称
    private String AreaCacheName;
}
