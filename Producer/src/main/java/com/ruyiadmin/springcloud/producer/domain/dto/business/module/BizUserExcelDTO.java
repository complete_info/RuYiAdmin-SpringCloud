package com.ruyiadmin.springcloud.producer.domain.dto.business.module;

import lombok.Data;


/**
 * <p>
 * 业务用户导入DTO模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-09-15
 */
@Data
public class BizUserExcelDTO {
    private String userLogonName;
    private String userDisplayName;
    private String userPassword;
    private String telephone;
    private String mobilephone;
    private String email;
    private String sex;
}
