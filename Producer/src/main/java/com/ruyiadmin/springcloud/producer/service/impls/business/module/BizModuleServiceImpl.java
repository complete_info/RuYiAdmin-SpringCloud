package com.ruyiadmin.springcloud.producer.service.impls.business.module;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.entity.business.module.BizModule;
import com.ruyiadmin.springcloud.producer.repository.business.module.IBizModuleRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.business.module.IBizModuleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 模块表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
@RequiredArgsConstructor
public class BizModuleServiceImpl extends ServiceImpl<IBizModuleRepository, BizModule> implements IBizModuleService {

    //region 实现类私有属性

    private final IBizModuleRepository bizModuleRepository;

    //endregion

    //region 查询无权限模块列表

    /**
     * 查询无权限模块列表
     *
     * @param userId 用户编号
     * @return QueryResult
     */
    @Override
    public QueryResult<BizModule> queryUserNonModules(String userId) {
        List<BizModule> list = this.bizModuleRepository.queryUserNonModules(userId);
        return QueryResult.success(list.size(), list);
    }

    //endregion
}
