package com.ruyiadmin.springcloud.producer.repository.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyiadmin.springcloud.producer.domain.dto.system.DbSchemaInfoDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 代码生成器 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-05
 */
@Mapper
public interface ICodeGeneratorRepository extends BaseMapper<DbSchemaInfoDTO> {
    /**
     * 查询数据库表空间信息
     *
     * @return 数据库空间模型集合
     */
    List<DbSchemaInfoDTO> queryMySqlSchemaInfo();

    /**
     * 查询数据库表字段信息
     *
     * @param tables 表名集合
     * @return 数据库空间模型集合
     */
    List<DbSchemaInfoDTO> queryMySqlSchemaColumnInfo(@Param("tables") List<String> tables);
}
