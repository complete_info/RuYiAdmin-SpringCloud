package com.ruyiadmin.springcloud.producer.common.beans.core;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 数据库配置工具类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-06
 */
@Data
@Component
public class DatasourceConfig {
    //数据库连接串
    @Value("${spring.shardingsphere.datasource.master.url}")
    private String url;

    //数据库用户名
    @Value("${spring.shardingsphere.datasource.master.username}")
    private String username;

    //数据库密码
    @Value("${spring.shardingsphere.datasource.master.password}")
    private String password;
}
