package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.ActionResult;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryCondition;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.LoginDTO;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysUserDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysUser;

import java.util.List;

/**
 * <p>
 * 系统用户 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-06-28
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 用户登录系统
     *
     * @param login LoginDTO
     * @return ActionResult
     * @throws Exception 异常
     */
    ActionResult logon(LoginDTO login) throws Exception;

    /**
     * 获取在线用户
     *
     * @return QueryResult
     */
    QueryResult<SysUserDTO> queryOnlineUsers();

    /**
     * 加载系统用户缓存
     */
    void loadSystemUserCache();

    /**
     * 清理系统用户缓存
     */
    void clearSystemUserCache();

    /**
     * 用户退出登录
     *
     * @param token 口令
     * @return ActionResult
     */
    ActionResult userLogout(String token);

    /**
     * 强制用户退出
     *
     * @param token 口令
     * @return ActionResult
     * @throws Exception 异常
     */
    ActionResult forceUserLogout(String token) throws Exception;

    /**
     * 获取机构用户
     *
     * @param queryCondition 查询条件
     * @return QueryResult
     */
    QueryResult<SysUserDTO> queryOrgUserInfo(QueryCondition queryCondition);

    /**
     * <p>
     * 查询所有用户信息
     * </p>
     *
     * @return 用户集合
     */
    List<SysUser> queryAllUsers();

}
