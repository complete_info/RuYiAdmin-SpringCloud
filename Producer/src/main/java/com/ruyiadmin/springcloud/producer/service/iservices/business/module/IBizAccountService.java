package com.ruyiadmin.springcloud.producer.service.iservices.business.module;

import com.ruyiadmin.springcloud.producer.domain.entity.business.module.BizAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 模块API访问账号表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface IBizAccountService extends IService<BizAccount> {

}
