package com.ruyiadmin.springcloud.producer.common.core.system.enums;

/**
 * <p>
 * 数据类型枚举
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-17
 */
public enum DataType {
    String,
    Date,
    DateTime,
    Guid,
    Int,
    Double
}
