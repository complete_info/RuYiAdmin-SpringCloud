package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruyiadmin.springcloud.producer.common.components.core.RuYiSessionContext;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryCondition;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysNotificationDTO;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysUserDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysAnnouncement;
import com.ruyiadmin.springcloud.producer.repository.system.ISysAnnouncementRepository;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysAnnouncementService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 通知公告表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
@Service
@RequiredArgsConstructor
public class SysAnnouncementServiceImpl extends ServiceImpl<ISysAnnouncementRepository, SysAnnouncement>
        implements ISysAnnouncementService {

    //region 实现类私有属性

    private final ISysAnnouncementRepository announcementRepository;
    private final RuYiSessionContext sessionContext;

    //endregion

    //region 查询通知列表

    /**
     * 查询通知列表
     *
     * @param queryCondition 查询条件
     * @return QueryResult
     * @throws Exception 异常
     */
    @Override
    public QueryResult<SysNotificationDTO> getNotifications(QueryCondition queryCondition) throws Exception {
        queryCondition.setSort(StringUtils.EMPTY);

        QueryWrapper<SysNotificationDTO> wrapper = new QueryWrapper<>();
        queryCondition.getQueryWrapper(wrapper);
        Page<SysNotificationDTO> page = new Page<>(queryCondition.getPageIndex(), queryCondition.getPageSize());

        SysUserDTO user = sessionContext.getCurrentUserInfo();
        IPage<SysNotificationDTO> records = this.announcementRepository.getNotifications(page, wrapper, user.getId());

        return QueryResult.success(records.getTotal(), records.getRecords());
    }

    //endregion
}
