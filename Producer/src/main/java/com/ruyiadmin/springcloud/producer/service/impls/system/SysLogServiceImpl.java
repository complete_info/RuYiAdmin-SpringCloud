package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysLog;
import com.ruyiadmin.springcloud.producer.repository.system.ISysLogRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 审计日志表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<ISysLogRepository, SysLog> implements ISysLogService {

}
