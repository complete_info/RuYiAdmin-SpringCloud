package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysOrgUser;
import com.ruyiadmin.springcloud.producer.repository.system.ISysOrgUserRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysOrgUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 机构用户关系表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
public class SysOrgUserServiceImpl extends ServiceImpl<ISysOrgUserRepository, SysOrgUser>
        implements ISysOrgUserService {

}
