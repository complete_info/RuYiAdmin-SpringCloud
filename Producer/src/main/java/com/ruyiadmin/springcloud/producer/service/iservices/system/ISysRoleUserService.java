package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysRoleUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色用户关系表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysRoleUserService extends IService<SysRoleUser> {

    /**
     * 加载角色与用户缓存
     */
    void loadSysRoleUserCache();

    /**
     * 清理角色与用户缓存
     */
    void clearSysRoleUserCache();

}
