package com.ruyiadmin.springcloud.producer.domain.entity.base;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.base.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 实体基类
 *
 * @author RuYiAdmin
 * @since 2022-06-21
 */
public class RuYiAdminBaseEntity extends BaseDomain {

}
