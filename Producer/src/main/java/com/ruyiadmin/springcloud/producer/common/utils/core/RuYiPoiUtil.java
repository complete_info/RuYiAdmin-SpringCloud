package com.ruyiadmin.springcloud.producer.common.utils.core;

import lombok.experimental.UtilityClass;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFComment;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Sheet;

import java.text.DecimalFormat;

/**
 * Poi工具类
 *
 * @author RuYiAdmin
 * @since 2022-08-13
 */
@UtilityClass
public class RuYiPoiUtil {

    //region 单元格添加批注

    /**
     * 单元格添加批注
     *
     * @param worksheet  工作簿
     * @param row        行
     * @param column     列
     * @param commentStr 注释
     */
    public void setCellComment(Sheet worksheet, int row, int column, String commentStr) {
        if (worksheet.getRow(row).getCell(column).getCellComment() == null) {
            HSSFPatriarch patr = (HSSFPatriarch) worksheet.createDrawingPatriarch();
            HSSFComment comment = (HSSFComment) patr.createCellComment(
                    new HSSFClientAnchor(0, 0, 0, 0,
                            (short) column, row - 1, (short) (column + 3), row + 4));
            comment.setString(new HSSFRichTextString(commentStr));
            comment.setAuthor("RuYiAdmin");
            worksheet.getRow(row).getCell(column).setCellComment(comment);
        } else {
            Comment comment = worksheet.getRow(row).getCell(column).getCellComment();
            RichTextString coStr = comment.getString();
            comment.setString(new HSSFRichTextString(commentStr + "\n" + coStr));
            comment.setAuthor("RuYiAdmin");
            worksheet.getRow(row).getCell(column).setCellComment(comment);
        }
    }

    //endregion

    //region 获取单元格的值

    /**
     * 获取单元格的值
     *
     * @param cell 单元格
     * @return 单元格的值
     */
    public String getCellValue(Cell cell) {
        if (cell == null) {
            return null;
        }
        String content = null;

        switch (cell.getCellType()) {
            case NUMERIC:   //数字或者时间
                Double doubleValue = cell.getNumericCellValue();
                // 格式化科学计数法，取一位整数
                DecimalFormat df = new DecimalFormat("0");
                content = df.format(doubleValue);
                break;
            case STRING:    //字符串
                content = cell.getStringCellValue();
                break;
            case BOOLEAN:   //布尔
                boolean booleanValue = cell.getBooleanCellValue();
                content = Boolean.toString(booleanValue);
                break;
            case BLANK:     // 空值
                break;
            case FORMULA:   // 公式
                content = cell.getCellFormula();
                break;
            case ERROR:     // 故障
                break;
            default:
                break;
        }

        return content;
    }

    //endregion

}
