package com.ruyiadmin.springcloud.producer.service.impls.system;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import com.ruyiadmin.springcloud.producer.common.beans.system.DirectoryConfig;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.ActionResult;
import com.ruyiadmin.springcloud.producer.common.utils.core.RuYiFileUtil;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysAttachment;
import com.ruyiadmin.springcloud.producer.repository.system.ISysAttachmentRepository;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysAttachmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 系统附件表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
@Service
@RequiredArgsConstructor
public class SysAttachmentServiceImpl extends ServiceImpl<ISysAttachmentRepository, SysAttachment> implements ISysAttachmentService {

    //region 实现类私有属性

    private final DirectoryConfig directoryConfig;

    //endregion

    //region 查询系统文件统计信息

    /**
     * 查询系统文件统计信息
     *
     * @return ActionResult
     */
    @Override
    public ActionResult querySysFileStatisticalInfo() {
        //临时文件
        int tempFileLength = FileUtil.ls(this.directoryConfig.getTempPath()).length;
        double tempFileSize = RuYiFileUtil.getDirectorySizeByMB(this.directoryConfig.getTempPath());

        //审计日志
        int monitoringLogLength = FileUtil.ls(this.directoryConfig.getMonitoringLogsPath()).length;
        double monitoringLogSize = RuYiFileUtil.getDirectorySizeByMB(this.directoryConfig.getMonitoringLogsPath());

        //业务附件
        int businessAttachmentLength = FileUtil.ls(this.directoryConfig.getBusinessAttachmentPath()).length;
        double businessAttachmentSize = RuYiFileUtil.getDirectorySizeByMB(this.directoryConfig.getBusinessAttachmentPath());

        String logPath = String.join("", System.getProperty("user.dir"), "/RuYiAdminLogs");
        String debugLogPath = String.join("", logPath, "/Debug");
        String infoLogPath = String.join("", logPath, "/Info");
        String errorLogPath = String.join("", logPath, "/Error");
        String warnLogPath = String.join("", logPath, "/Warn");

        //Debug文件
        int debugLogLength = FileUtil.ls(debugLogPath).length;
        double debugLogSize = RuYiFileUtil.getDirectorySizeByMB(debugLogPath);

        //Error文件
        int errorLogLength = FileUtil.ls(errorLogPath).length;
        double errorLogSize = RuYiFileUtil.getDirectorySizeByMB(errorLogPath);

        //Info文件
        int infoLogLength = FileUtil.ls(infoLogPath).length;
        double infoLogSize = RuYiFileUtil.getDirectorySizeByMB(infoLogPath);

        //Warn文件
        int warnLogLength = FileUtil.ls(warnLogPath).length;
        double warnLogSize = RuYiFileUtil.getDirectorySizeByMB(warnLogPath);

        Map<String, Object> map = new HashMap<>();
        map.put("tempFileLength", tempFileLength);
        map.put("tempFileSize", tempFileSize);
        map.put("monitoringLogLength", monitoringLogLength);
        map.put("monitoringLogSize", monitoringLogSize);
        map.put("businessAttachmentLength", businessAttachmentLength);
        map.put("businessAttachmentSize", businessAttachmentSize);
        map.put("debugLogLength", debugLogLength);
        map.put("debugLogSize", debugLogSize);
        map.put("errorLogLength", errorLogLength);
        map.put("errorLogSize", errorLogSize);
        map.put("infoLogLength", infoLogLength);
        map.put("infoLogSize", infoLogSize);
        map.put("warnLogLength", warnLogLength);
        map.put("warnLogSize", warnLogSize);

        return ActionResult.success(JSON.toJSON(map));
    }

    //endregion

}
