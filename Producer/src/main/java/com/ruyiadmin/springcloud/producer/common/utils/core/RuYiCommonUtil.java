package com.ruyiadmin.springcloud.producer.common.utils.core;

import lombok.experimental.UtilityClass;

import java.util.UUID;

/**
 * 通用工具类
 *
 * @author RuYiAdmin
 * @since 2022-07-20
 */
@UtilityClass
public class RuYiCommonUtil {

    //region 判断是否为UUID

    /**
     * 判断是否为UUID
     *
     * @param uuid 参数
     * @return 真假值
     */
    public boolean isUuid(String uuid) {
        try {
            UUID.fromString(uuid);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    //endregion

}
