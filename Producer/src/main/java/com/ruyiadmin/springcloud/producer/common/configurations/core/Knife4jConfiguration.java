package com.ruyiadmin.springcloud.producer.common.configurations.core;

import com.github.xiaoymin.knife4j.spring.extension.OpenApiExtensionResolver;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * <p>
 * SpringDocument配置
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-09-23
 */
@Configuration
@EnableSwagger2WebMvc
@RequiredArgsConstructor
public class Knife4jConfiguration {
    private final OpenApiExtensionResolver openApiExtensionResolver;

    //region 接口文档配置

    @Bean
    public Docket docket() {
        String groupName = "RuYiAdmin-SpringCloud-WebAPI";//分组名称
        String host = "https://gitee.com/pang-mingjun/RuYiAdmin-SpringCloud";//主机名
        return new Docket(DocumentationType.SWAGGER_2)
                .host(host)
                .apiInfo(apiInfo())
                .groupName(groupName)
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build()
                .extensions(openApiExtensionResolver.buildExtensions(groupName));
    }

    //endregion

    //region 接口说明信息

    private ApiInfo apiInfo() {
        String title = "RuYiAdmin SpringCloud WebApi Documentation";//标题
        String description = "版权所有、未经授权、禁止商用、侵权必究";//简介
        String termsOfServiceUrl = "https://gitee.com/pang-mingjun/RuYiAdmin-SpringCloud/blob/master/LICENSE";//服务条款
        String contactName = "不老的传说";//联系人
        String contactUrl = "https://gitee.com/pang-mingjun/RuYiAdmin-SpringCloud";//联系网址
        String contactEmail = "983810803@qq.com";//联系邮箱
        String version = "V-1.0";//版本号
        return new ApiInfoBuilder()
                .title(title)
                .description(description)
                .termsOfServiceUrl(termsOfServiceUrl)
                .contact(new Contact(contactName, contactUrl, contactEmail))
                .version(version)
                .build();
    }

    //endregion

}
