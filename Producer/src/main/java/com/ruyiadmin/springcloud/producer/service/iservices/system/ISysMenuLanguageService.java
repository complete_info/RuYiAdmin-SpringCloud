package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysMenuLanguage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单多语表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysMenuLanguageService extends IService<SysMenuLanguage> {

    /**
     * 加载菜单与多语缓存
     */
    void loadSysMenuLanguageCache();

    /**
     * 清理菜单与多语缓存
     */
    void clearSysMenuLanguageCache();

}
