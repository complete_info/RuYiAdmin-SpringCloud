package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysMenuDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysMenuService extends IService<SysMenu> {
    /**
     * 加载系统菜单缓存
     */
    void loadSysMenuCache();

    /**
     * 清理系统菜单缓存
     */
    void clearSysMenuCache();

    /**
     * 查询菜单列表
     *
     * @return QueryResult
     */
    QueryResult<SysMenuDTO> getMenuTreeNodes();
}
