package com.ruyiadmin.springcloud.producer.common.constants.system;

/**
 * <p>
 * RuYiAdmin Guid常量
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-04
 */
public class Guid {
    public static final String Empty = "00000000-0000-0000-0000-000000000000";
}
