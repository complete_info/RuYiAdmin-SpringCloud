package com.ruyiadmin.springcloud.producer.domain.entity.system;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.ruyiadmin.springcloud.producer.domain.entity.base.RuYiAdminBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 导入配置明细表模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Getter
@Setter
@TableName("sys_import_config_detail")
@ApiModel(value = "SysImportConfigDetail对象", description = "导入配置明细表")
public class SysImportConfigDetail extends RuYiAdminBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId("ID")
    @NotNull
    private String id;

    @ApiModelProperty("父键")
    @TableField("PARENT_ID")
    @NotNull
    private String parentId;

    @ApiModelProperty("数据类型，0：小数，1：整数，2：文本t，3：日期，4：时间")
    @TableField("DATA_TYPE")
    @NotNull
    private Integer dataType;

    @ApiModelProperty("所在列")
    @TableField("CELLS")
    @NotNull
    @Max(512)
    private String cells;

    @ApiModelProperty("是否必填项，0：否，1：是")
    @TableField("REQUIRED")
    private Integer required;

    @ApiModelProperty("最大值")
    @TableField("MAX_VALUE")
    private Double maxValue;

    @ApiModelProperty("最小值")
    @TableField("MIN_VALUE")
    private Double minValue;

    @ApiModelProperty("小数位上限")
    @TableField("DECIMAL_LIMIT")
    private Integer decimalLimit;

    @ApiModelProperty("枚举列表")
    @TableField("TEXT_ENUM")
    private String textEnum;

    @ApiModelProperty("扩展字段")
    @TableField("EXTEND1")
    private String extend1;

    @ApiModelProperty("扩展字段")
    @TableField("EXTEND2")
    private String extend2;

    @ApiModelProperty("扩展字段")
    @TableField("EXTEND3")
    private String extend3;

    @ApiModelProperty("序号")
    @TableField("SERIAL_NUMBER")
    private Integer serialNumber;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @TableLogic
    @NotNull
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

}
