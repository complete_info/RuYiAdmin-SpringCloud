package com.ruyiadmin.springcloud.producer.repository.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysAddressee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 收件人表 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
@Mapper
public interface ISysAddresseeRepository extends BaseMapper<SysAddressee> {

}
