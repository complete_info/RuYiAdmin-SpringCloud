package com.ruyiadmin.springcloud.producer.common.exceptions;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * RuYiAdmin自定义异常
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-15
 */
@Slf4j
public class RuYiAdminCustomException extends RuntimeException {

    /**
     * 无参构造
     */
    public RuYiAdminCustomException() {
        super();
        this.printStackTrace();
        log.error(this.getMessage());
    }

    /**
     * 有参构造
     *
     * @param message 异常消息
     */
    public RuYiAdminCustomException(String message) {
        super(message);
        this.printStackTrace();
        log.error(message);
    }

    /**
     * 有参构造
     *
     * @param message 异常消息
     * @param cause   Throwable对象
     */
    public RuYiAdminCustomException(String message, Throwable cause) {
        super(message, cause);
        this.printStackTrace();
        log.error(message);
    }

    /**
     * 有参构造
     *
     * @param cause Throwable对象
     */
    public RuYiAdminCustomException(Throwable cause) {
        super(cause);
        this.printStackTrace();
        log.error(cause.getMessage());
    }

    /**
     * 受保护的有参构造
     *
     * @param message            异常消息
     * @param cause              Throwable对象
     * @param enableSuppression  boolean
     * @param writableStackTrace boolean
     */
    protected RuYiAdminCustomException(String message,
                                       Throwable cause,
                                       boolean enableSuppression,
                                       boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.printStackTrace();
        log.error(message);
    }

}
