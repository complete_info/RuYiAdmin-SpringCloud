package com.ruyiadmin.springcloud.producer.common.configurations.core;

import com.alibaba.csp.sentinel.annotation.aspectj.SentinelResourceAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 * RuYiAdmin AspectJ配置
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-10-18
 */
@Configuration
public class SentinelAspectConfiguration {
    @Bean
    public SentinelResourceAspect sentinelResourceAspect() {
        return new SentinelResourceAspect();
    }
}
