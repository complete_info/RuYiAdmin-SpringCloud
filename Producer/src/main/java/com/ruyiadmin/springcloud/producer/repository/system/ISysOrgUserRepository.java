package com.ruyiadmin.springcloud.producer.repository.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysOrgUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 机构用户关系表 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Mapper
public interface ISysOrgUserRepository extends BaseMapper<SysOrgUser> {

}
