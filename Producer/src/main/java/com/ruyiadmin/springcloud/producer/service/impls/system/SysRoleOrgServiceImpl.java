package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.alibaba.fastjson.JSON;
import com.ruyiadmin.springcloud.producer.common.beans.system.SystemCacheConfig;
import com.ruyiadmin.springcloud.producer.common.components.core.RuYiRedisComponent;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysRoleOrg;
import com.ruyiadmin.springcloud.producer.repository.system.ISysRoleOrgRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysRoleOrgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 角色机构关系表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SysRoleOrgServiceImpl extends ServiceImpl<ISysRoleOrgRepository, SysRoleOrg>
        implements ISysRoleOrgService {

    //region 实现类私有属性

    private final RuYiRedisComponent redisUtils;
    private final SystemCacheConfig systemCacheConfig;

    //endregion

    //region 加载角色与机构缓存

    /**
     * 加载角色与机构缓存
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void loadSysRoleOrgCache() {
        List<SysRoleOrg> list = this.list();
        this.redisUtils.set(systemCacheConfig.getRoleAndOrgCacheName(), JSON.toJSONString(list));
        log.info("RuYiAdmin sys roles and orgs cache loaded");
    }

    //endregion

    //region 清理角色与机构缓存

    /**
     * 清理角色与机构缓存
     */
    @Override
    public void clearSysRoleOrgCache() {
        this.redisUtils.del(systemCacheConfig.getRoleAndOrgCacheName());
        log.info("RuYiAdmin sys roles and orgs cache cleared");
    }

    //endregion

}
