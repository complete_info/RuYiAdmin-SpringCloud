package com.ruyiadmin.springcloud.producer.domain.dto.system;

import lombok.Data;

/**
 * 代码生成器DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-08-06
 */
@Data
public class CodeGeneratorDTO {
    //表数组
    private String tables;
    //布局方式，0:上下布局，1:左右布局
    private int layoutMode;
}
