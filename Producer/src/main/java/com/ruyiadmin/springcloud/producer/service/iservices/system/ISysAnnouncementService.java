package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryCondition;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysNotificationDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysAnnouncement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 通知公告表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
public interface ISysAnnouncementService extends IService<SysAnnouncement> {
    /**
     * 查询通知列表
     *
     * @param queryCondition 查询条件
     * @return QueryResult
     * @throws Exception 异常
     */
    QueryResult<SysNotificationDTO> getNotifications(QueryCondition queryCondition) throws Exception;
}
