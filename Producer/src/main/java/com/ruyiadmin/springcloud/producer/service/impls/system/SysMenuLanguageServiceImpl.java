package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.alibaba.fastjson.JSON;
import com.ruyiadmin.springcloud.producer.common.beans.system.SystemCacheConfig;
import com.ruyiadmin.springcloud.producer.common.components.core.RuYiRedisComponent;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysMenuLanguage;
import com.ruyiadmin.springcloud.producer.repository.system.ISysMenuLanguageRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysMenuLanguageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 菜单多语表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SysMenuLanguageServiceImpl extends ServiceImpl<ISysMenuLanguageRepository,
        SysMenuLanguage> implements ISysMenuLanguageService {

    //region 实现类私有属性

    private final RuYiRedisComponent redisUtils;
    private final SystemCacheConfig systemCacheConfig;

    //endregion

    //region 加载菜单与多语缓存

    /**
     * 加载菜单与多语缓存
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void loadSysMenuLanguageCache() {
        List<SysMenuLanguage> list = this.list();
        this.redisUtils.set(systemCacheConfig.getMenuAndLanguageCacheName(), JSON.toJSONString(list));
        log.info("RuYiAdmin sys menus and languages cache loaded");
    }

    //endregion

    //region 清理菜单与多语缓存

    /**
     * 清理菜单与多语缓存
     */
    @Override
    public void clearSysMenuLanguageCache() {
        this.redisUtils.del(systemCacheConfig.getMenuAndLanguageCacheName());
        log.info("RuYiAdmin sys menus and languages cache cleared");
    }

    //endregion

}
