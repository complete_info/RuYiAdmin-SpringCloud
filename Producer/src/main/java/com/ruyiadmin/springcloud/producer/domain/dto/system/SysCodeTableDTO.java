package com.ruyiadmin.springcloud.producer.domain.dto.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysCodeTable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 数据字典DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-07-17
 */
@Data
public class SysCodeTableDTO extends SysCodeTable {
    @ApiModelProperty("数据字典子集")
    private List<SysCodeTableDTO> children;
}
