package com.ruyiadmin.springcloud.producer.repository.business.module;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyiadmin.springcloud.producer.domain.entity.business.module.BizAccount;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 模块API访问账号表 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Mapper
public interface IBizAccountRepository extends BaseMapper<BizAccount> {

}
