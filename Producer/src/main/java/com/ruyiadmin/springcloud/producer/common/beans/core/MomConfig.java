package com.ruyiadmin.springcloud.producer.common.beans.core;

import com.ruyiadmin.springcloud.producer.common.core.system.enums.MomType;
import com.ruyiadmin.springcloud.producer.common.factories.RuYiAdminYamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 消息中间件配置类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-17
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "momconfig")
@PropertySource(value = {"classpath:appSettings.yaml"}, factory = RuYiAdminYamlPropertySourceFactory.class)
@Data
public class MomConfig {
    //消息中间件类型
    private int MomType;
}
