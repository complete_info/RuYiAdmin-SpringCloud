package com.ruyiadmin.springcloud.producer.controller.system;

import com.ruyiadmin.springcloud.producer.common.annotations.system.Log;
import com.ruyiadmin.springcloud.producer.common.core.business.enums.OperationType;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.ActionResult;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.CodeGeneratorDTO;
import com.ruyiadmin.springcloud.producer.domain.dto.system.DbSchemaInfoDTO;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ICodeGeneratorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * <p>
 * 代码生成器 前端控制器
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-05
 */
@RestController
@RequestMapping("/CodeGenerator")
@Api(tags = "系统代码生成器服务")
@RequiredArgsConstructor
public class CodeGeneratorController {

    private final ICodeGeneratorService codeGeneratorService;

    //region 获取表空间信息

    @PostMapping("/Post")
    @ApiOperation(value = "获取表空间信息")
    @Log(OperationType = OperationType.QueryList)
    public QueryResult<DbSchemaInfoDTO> querySchemaInfo() throws ExecutionException, InterruptedException {
        CompletableFuture<QueryResult<DbSchemaInfoDTO>> future =
                CompletableFuture.supplyAsync(this.codeGeneratorService::querySchemaInfo);
        return future.get();
    }

    //endregion

    //region 生成项目代码

    @PostMapping("/CodeGenerate")
    @ApiOperation(value = "生成项目代码")
    @Log(OperationType = OperationType.GenerateCode)
    public ActionResult codeGenerate(@RequestBody CodeGeneratorDTO codeGenerator) throws IOException, ExecutionException, InterruptedException {
        CompletableFuture<ActionResult> future =
                CompletableFuture.supplyAsync(() -> {
                    try {
                        return this.codeGeneratorService.codeGenerate(codeGenerator);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                });
        return future.get();
    }

    //endregion

}
