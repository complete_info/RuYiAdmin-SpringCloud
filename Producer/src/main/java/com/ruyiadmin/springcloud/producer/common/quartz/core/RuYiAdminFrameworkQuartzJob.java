package com.ruyiadmin.springcloud.producer.common.quartz.core;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.ruyiadmin.springcloud.producer.common.beans.system.DirectoryConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * <p>
 * 系统定时任务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-02
 */
@Slf4j
@RequiredArgsConstructor
public class RuYiAdminFrameworkQuartzJob extends QuartzJobBean {

    private final DirectoryConfig directoryConfig;

    @Override
    protected void executeInternal(@NotNull JobExecutionContext jobExecutionContext) throws JobExecutionException {
        int hour = LocalDateTimeUtil.now().getHour();
        int minute = LocalDateTimeUtil.now().getMinute();
        if (hour == 4 && minute == 0) {
            try {
                //清理临时文件、释放服务器存储空间
                this.directoryConfig.cleanTempPath();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        String msg = (String) jobExecutionContext.getJobDetail().getJobDataMap().get("msg");
        log.info("{}", msg);
    }

}
