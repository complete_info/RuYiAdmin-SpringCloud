package com.ruyiadmin.springcloud.producer.common.utils.core;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import lombok.experimental.UtilityClass;

/**
 * Date工具类
 *
 * @author RuYiAdmin
 * @since 2022-08-13
 */
@UtilityClass
public class RuYiDateUtil {

    //region 判断是否为日期

    /**
     * 判断是否为日期
     *
     * @param obj 输入
     * @return 真假值
     */
    public boolean isDate(Object obj) {
        boolean result = false;

        if (obj != null) {
            try {
                DateUtil.parse(obj.toString());
                result = true;
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        return result;
    }

    //endregion

    //region 判断是否为日期+时间

    /**
     * 判断是否为日期+时间
     *
     * @param obj 输入
     * @return 真假值
     */
    public boolean isDateTime(Object obj) {
        boolean result = false;

        if (obj != null) {
            try {
                new DateTime(obj.toString());
                result = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    //endregion

}
