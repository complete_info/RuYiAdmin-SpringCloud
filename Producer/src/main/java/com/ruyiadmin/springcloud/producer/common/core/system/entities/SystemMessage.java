package com.ruyiadmin.springcloud.producer.common.core.system.entities;

import com.ruyiadmin.springcloud.producer.common.core.system.enums.MessageType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 系统消息
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-29
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SystemMessage {
    //消息
    private String Message;
    //消息类型
    private MessageType MessageType;
    //信息载体
    private Object Object;
}
