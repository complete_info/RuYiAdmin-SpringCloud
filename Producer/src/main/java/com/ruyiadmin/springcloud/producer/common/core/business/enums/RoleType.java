package com.ruyiadmin.springcloud.producer.common.core.business.enums;

/**
 * <p>
 * 角色类型枚举
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-12
 */
public enum RoleType {
    //自有的
    Own,
    //继承的
    Inherited
}
