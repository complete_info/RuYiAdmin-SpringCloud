package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysImportConfigDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 导入配置子表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysImportConfigDetailService extends IService<SysImportConfigDetail> {

}
