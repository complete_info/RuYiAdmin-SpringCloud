package com.ruyiadmin.springcloud.producer.service.impls.business.module;

import com.ruyiadmin.springcloud.producer.domain.entity.business.module.BizAccount;
import com.ruyiadmin.springcloud.producer.repository.business.module.IBizAccountRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.business.module.IBizAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 模块API访问账号表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
public class BizAccountServiceImpl extends ServiceImpl<IBizAccountRepository, BizAccount> implements IBizAccountService {

}
