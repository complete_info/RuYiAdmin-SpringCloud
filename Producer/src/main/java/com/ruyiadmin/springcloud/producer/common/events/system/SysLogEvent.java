package com.ruyiadmin.springcloud.producer.common.events.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysLog;
import org.springframework.context.ApplicationEvent;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * <p>
 * 系统日志事件
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-20
 */
public class SysLogEvent extends ApplicationEvent {

    public SysLogEvent(SysLog source) {
        super(source);
        // 将RequestAttributes对象设置为子线程共享
        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        RequestContextHolder.setRequestAttributes(sra, true);
    }

}
