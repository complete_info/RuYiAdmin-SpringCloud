package com.ruyiadmin.springcloud.producer.domain.dto.business.module;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 业务用户DTO模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-09-08
 */
@Data
public class BizUserDTO {

    @ApiModelProperty("模块编号")
    @TableField("MODULE_ID")
    @NotNull
    private String moduleId;

    @ApiModelProperty("用户登录账号")
    @TableField("USER_LOGON_NAME")
    @NotNull
    @Max(128)
    private String userLogonName;

    @ApiModelProperty("用户名称")
    @TableField("USER_DISPLAY_NAME")
    @NotNull
    @Max(128)
    private String userDisplayName;

    @ApiModelProperty("用户密码")
    @TableField("USER_PASSWORD")
    @NotNull
    @Max(512)
    private String userPassword;

    @ApiModelProperty("座机")
    @TableField("TELEPHONE")
    @Max(45)
    private String telephone;

    @ApiModelProperty("手机")
    @TableField("MOBILEPHONE")
    @Max(45)
    private String mobilephone;

    @ApiModelProperty("邮箱")
    @TableField("EMAIL")
    @Max(45)
    private String email;

    @ApiModelProperty("性别，男：0，女：1，第三性别：2")
    @TableField("SEX")
    @NotNull
    private Integer sex;

}
