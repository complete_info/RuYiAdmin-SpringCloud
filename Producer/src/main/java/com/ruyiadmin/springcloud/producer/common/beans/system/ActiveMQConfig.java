package com.ruyiadmin.springcloud.producer.common.beans.system;

import com.ruyiadmin.springcloud.producer.common.factories.RuYiAdminYamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 系统ActiveMQ配置类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-29
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "activemqconfig")
@PropertySource(value = {"classpath:appSettings.yaml"}, factory = RuYiAdminYamlPropertySourceFactory.class)
@Data
public class ActiveMQConfig {
    //默认主题名称
    private String TopicName;
    //默认队列名称
    private String QueueName;
}
