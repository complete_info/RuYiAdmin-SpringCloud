package com.ruyiadmin.springcloud.producer.common.beans.system;

import com.ruyiadmin.springcloud.producer.common.factories.RuYiAdminYamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * SM2配置类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-10-09
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "sm2config")
@PropertySource(value = {"classpath:appSettings.yaml"}, factory = RuYiAdminYamlPropertySourceFactory.class)
@Data
public class SM2Config {
    //公钥
    private String PublicKey;
    //私钥
    private String PrivateKey;
}
