package com.ruyiadmin.springcloud.producer.domain.dto.system;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Db Schema Field DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-08-07
 */
@Data
public class DbSchemaFieldDTO {
    @ApiModelProperty("字段名称")
    @TableField("COLUMN_NAME")
    private String columnName;

    @ApiModelProperty("数据类型")
    @TableField("DATA_TYPE")
    private String dataType;

    @ApiModelProperty("字段注释")
    @TableField("COLUMN_COMMENT")
    private String columnComment;

    @ApiModelProperty("是否为空")
    @TableField("IS_NULLABLE")
    private String isNullable;

    @ApiModelProperty("最大长度")
    @TableField("CHARACTER_MAXIMUM_LENGTH")
    private String characterMaximumLength;
}
