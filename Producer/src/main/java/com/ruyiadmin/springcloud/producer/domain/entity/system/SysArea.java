package com.ruyiadmin.springcloud.producer.domain.entity.system;

import com.baomidou.mybatisplus.annotation.*;
import com.ruyiadmin.springcloud.producer.domain.entity.base.RuYiAdminBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 行政区域实体模型
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
@TableName("sys_area")
@ApiModel(value = "SysArea对象", description = "行政区域表")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysArea extends RuYiAdminBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("ID")
    @ApiModelProperty("主键")
    @NotNull
    private String id;

    @TableField("AREA_CODE")
    @ApiModelProperty("地区编码")
    @NotNull
    @Max(6)
    private String AreaCode;

    @TableField("PARENT_AREA_CODE")
    @ApiModelProperty("父地区编码")
    @NotNull
    @Max(6)
    private String ParentAreaCode;

    @TableField("AREA_NAME")
    @ApiModelProperty("地区名称")
    @NotNull
    @Max(50)
    private String AreaName;

    @TableField("ZIP_CODE")
    @ApiModelProperty("邮政编码")
    @NotNull
    @Max(50)
    private String ZipCode;

    @TableField("AREA_LEVEL")
    @ApiModelProperty("地区层级(1省份 2城市 3区县)")
    @NotNull
    private int AreaLevel;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @TableLogic
    @NotNull
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

}
