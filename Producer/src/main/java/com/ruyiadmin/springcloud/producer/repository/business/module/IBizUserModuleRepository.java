package com.ruyiadmin.springcloud.producer.repository.business.module;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruyiadmin.springcloud.producer.domain.dto.business.module.BizUserModuleDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.business.module.BizUserModule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 模块与用户关系表 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Mapper
public interface IBizUserModuleRepository extends BaseMapper<BizUserModule> {

    /**
     * <p>
     * 分页查询业务用户与模块信息
     * </p>
     *
     * @param page    分页信息
     * @param wrapper 查询条件
     * @return 分页集合
     */
    IPage<BizUserModuleDTO> queryUserModule(Page<BizUserModuleDTO> page,
                                            @Param(Constants.WRAPPER) QueryWrapper<BizUserModuleDTO> wrapper);
}
