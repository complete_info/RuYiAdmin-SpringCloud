package com.ruyiadmin.springcloud.producer.domain.dto.system;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 登录DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-08-05
 */
@Data
public class LoginDTO {

    //登录账号
    @NotNull(message = "can not be null")
    @NotBlank(message = "can not be null")
    @NotEmpty(message = "can not be null")
    @JsonProperty(value = "UserName")
    private String userName;

    //密码
    @NotNull(message = "can not be null")
    @NotBlank(message = "can not be null")
    @NotEmpty(message = "can not be null")
    @JsonProperty(value = "Password")
    private String password;

    //验证码编号
    @NotNull(message = "can not be null")
    @NotBlank(message = "can not be null")
    @NotEmpty(message = "can not be null")
    @JsonProperty(value = "CaptchaId")
    private String captchaId;

    //验证码
    @NotNull(message = "can not be null")
    @NotBlank(message = "can not be null")
    @NotEmpty(message = "can not be null")
    @JsonProperty(value = "Captcha")
    private String captcha;

}
