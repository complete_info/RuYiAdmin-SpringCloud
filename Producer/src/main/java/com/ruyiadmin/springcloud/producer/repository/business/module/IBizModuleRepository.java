package com.ruyiadmin.springcloud.producer.repository.business.module;

import com.ruyiadmin.springcloud.producer.domain.entity.business.module.BizModule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 模块表 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Mapper
public interface IBizModuleRepository extends BaseMapper<BizModule> {

    /**
     * 查询无权限模块列表
     *
     * @param userId 用户编号
     * @return 模块集合
     */
    List<BizModule> queryUserNonModules(@Param("userId") String userId);
}
