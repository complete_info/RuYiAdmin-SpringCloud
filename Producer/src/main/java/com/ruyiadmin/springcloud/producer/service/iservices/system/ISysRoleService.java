package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryCondition;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysRoleDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysRoleService extends IService<SysRole> {

    /**
     * 加载系统角色缓存
     */
    void loadSysRoleCache();

    /**
     * 清理系统角色缓存
     */
    void clearSysRoleCache();

    /**
     * 查询机构角色
     *
     * @param queryCondition 查询条件
     * @return QueryResult
     */
    QueryResult<SysRoleDTO> queryOrgRoleInfo(QueryCondition queryCondition);

}
