package com.ruyiadmin.springcloud.producer.controller.core;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统Nacos配置服务 前端控制器
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-03
 */
@RestController
@RequestMapping("/RuYiAdminNacos")
@Api(tags = "系统Nacos配置服务")
public class RuYiAdminNacosController {

    @NacosValue(value = "${environmentName}", autoRefreshed = true)
    private String environmentName;

    //region 查询环境名称

    @GetMapping("/GetEnvironmentName")
    @ApiOperation(value = "查询环境名称")
    public String getEnvironmentName() {
        return environmentName;
    }

    //endregion

}
