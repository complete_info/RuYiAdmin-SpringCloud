package com.ruyiadmin.springcloud.producer.common.classes.system;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 系统验证码类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileInfo {
    //文件大小
    private double size;
    //单位
    private String remark;
}
