package com.ruyiadmin.springcloud.producer.repository.system;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysUserDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统用户 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
@Mapper
public interface ISysUserRepository extends BaseMapper<SysUser> {

    /**
     * 查询所有用户信息
     *
     * @return 用户信息集合
     */
    List<SysUserDTO> queryAllSysUser();

    /**
     * 分页查询当前机构用户
     *
     * @param page         分页信息
     * @param queryWrapper 过滤条件
     * @return 分页用户信息
     */
    IPage<SysUserDTO> queryOrgUserInfo(IPage<SysUserDTO> page,
                                       @Param(Constants.WRAPPER) Wrapper<SysUserDTO> queryWrapper);

    /**
     * <p>
     * 查询所有用户信息
     * </p>
     *
     * @return 用户集合
     */
    List<SysUser> queryAllUsers();
}
