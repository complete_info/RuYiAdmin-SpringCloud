package com.ruyiadmin.springcloud.producer.domain.entity.business.module;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.ruyiadmin.springcloud.producer.domain.entity.base.RuYiAdminBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 模块与用户关系模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Getter
@Setter
@TableName("biz_user_module")
@ApiModel(value = "BizUserModule对象", description = "模块与用户关系表")
@NoArgsConstructor
@AllArgsConstructor
public class BizUserModule extends RuYiAdminBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId("ID")
    @NotNull
    private String id;

    @ApiModelProperty("用户编号")
    @TableField("USER_ID")
    @NotNull
    private String userId;

    @ApiModelProperty("模块编号")
    @TableField("MODULE_ID")
    @NotNull
    private String moduleId;

    @ApiModelProperty("用户所在模块登录账号")
    @TableField("USER_MODULE_LOGON_NAME")
    @NotNull
    @Max(128)
    private String userModuleLogonName;

    @ApiModelProperty("用户所在模块登录密码")
    @TableField("USER_MODULE_PASSWORD")
    @NotNull
    @Max(512)
    private String userModulePassword;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @TableLogic
    @NotNull
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

}
