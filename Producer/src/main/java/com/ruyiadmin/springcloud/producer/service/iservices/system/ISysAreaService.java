package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysAreaDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysArea;

/**
 * <p>
 * 行政区域 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
public interface ISysAreaService extends IService<SysArea> {

    /**
     * 加载行政区域缓存
     */
    void loadSysAreaCache();

    /**
     * 清理行政区域缓存
     */
    void clearSysAreaCache();

    /**
     * 获取行政区域树形结构
     *
     * @return QueryResult
     */
    QueryResult<SysAreaDTO> getAreaTreeNodes();
}
