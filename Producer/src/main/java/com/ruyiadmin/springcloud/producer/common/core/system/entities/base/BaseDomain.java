package com.ruyiadmin.springcloud.producer.common.core.system.entities.base;

import com.alibaba.fastjson.JSON;
import lombok.ToString;

@ToString
public class BaseDomain {
    public String toJson() {
        return JSON.toJSONString(this);
    }
}
