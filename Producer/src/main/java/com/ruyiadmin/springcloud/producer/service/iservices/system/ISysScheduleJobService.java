package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysScheduleJob;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 计划任务表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysScheduleJobService extends IService<SysScheduleJob> {

    /**
     * 启动计划任务
     *
     * @param jobId  计划任务编号
     * @param userId 用户编号
     */
    void startScheduleJob(String jobId, String userId);

    /**
     * 暂停计划任务
     *
     * @param jobId  计划任务编号
     * @param userId 用户编号
     */
    void pauseScheduleJob(String jobId, String userId);

    /**
     * 恢复计划任务
     *
     * @param jobId  计划任务编号
     * @param userId 用户编号
     */
    void resumeScheduleJob(String jobId, String userId);

    /**
     * 删除计划任务
     *
     * @param jobId  计划任务编号
     * @param userId 用户编号
     */
    void deleteScheduleJob(String jobId, String userId);

    /**
     * 启动系统计划任务
     */
    void startScheduleJob();

    /**
     * 加载计划任务缓存
     */
    void loadBusinessScheduleJobCache();

    /**
     * 清理计划任务缓存
     */
    void clearBusinessScheduleJobCache();

}
