package com.ruyiadmin.springcloud.producer.repository.system;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysRoleDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Mapper
public interface ISysRoleRepository extends BaseMapper<SysRole> {

    /**
     * 查询所有系统角色信息
     *
     * @return 角色集合
     */
    List<SysRoleDTO> queryAllSysRole();

    /**
     * 分页查询当前机构角色
     *
     * @param page         分页信息
     * @param queryWrapper 过滤条件
     * @return 角色分页信息
     */
    IPage<SysRoleDTO> queryOrgRoleInfo(IPage<SysRoleDTO> page,
                                       @Param(Constants.WRAPPER) Wrapper<SysRoleDTO> queryWrapper);

}
