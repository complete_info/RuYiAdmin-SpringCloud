package com.ruyiadmin.springcloud.producer.domain.entity.system;

import com.baomidou.mybatisplus.annotation.*;
import com.ruyiadmin.springcloud.producer.domain.entity.base.RuYiAdminBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户实体模型
 *
 * @author RuYiAdmin
 * @since 2022-06-21
 */
@TableName("sys_user")
@ApiModel(value = "SysUser对象", description = "用户表")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUser extends RuYiAdminBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("ID")
    @ApiModelProperty("主键")
    @NotNull
    private String id;

    @TableField("LOGON_NAME")
    @ApiModelProperty("登录账号")
    @NotNull
    @Max(128)
    private String logonName;

    @TableField("DISPLAY_NAME")
    @ApiModelProperty("用户名")
    @NotNull
    @Max(128)
    private String displayName;

    @TableField("PASSWORD")
    @ApiModelProperty("用户密码")
    @NotNull
    @Max(512)
    private String password;

    @TableField("TELEPHONE")
    @ApiModelProperty("座机号")
    @Max(45)
    private String telephone;

    @TableField("MOBILEPHONE")
    @ApiModelProperty("手机号")
    @Max(45)
    private String mobilephone;

    @TableField("EMAIL")
    @ApiModelProperty("电子邮件")
    @Max(45)
    private String email;

    @TableField("IS_SUPPER_ADMIN")
    @ApiModelProperty("是否超级用户")
    private int isSupperAdmin;

    @TableField("SERIAL_NUMBER")
    @ApiModelProperty("序号")
    private int serialNumber;

    @TableField("IS_ENABLED")
    @ApiModelProperty("是否启用，0：禁用，1：启用")
    private int isEnabled;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @TableLogic
    @NotNull
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

    @ApiModelProperty("性别，男：0，女：1，第三性别：2")
    @TableField("SEX")
    @NotNull
    private Integer sex;

    @ApiModelProperty("盐")
    @TableField("SALT")
    @NotNull
    private String salt;

    @ApiModelProperty("预留字段1")
    @TableField("EXTEND1")
    @Max(36)
    private String extend1;

    @ApiModelProperty("预留字段2")
    @TableField("EXTEND2")
    @Max(64)
    private String extend2;

    @ApiModelProperty("预留字段3")
    @TableField("EXTEND3")
    @Max(128)
    private String extend3;

    @ApiModelProperty("预留字段4")
    @TableField("EXTEND4")
    @Max(256)
    private String extend4;

    @ApiModelProperty("预留字段5")
    @TableField("EXTEND5")
    @Max(512)
    private String extend5;

}

