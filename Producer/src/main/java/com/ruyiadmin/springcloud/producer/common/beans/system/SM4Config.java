package com.ruyiadmin.springcloud.producer.common.beans.system;

import com.ruyiadmin.springcloud.producer.common.factories.RuYiAdminYamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * SM4配置类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-10-09
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "sm4config")
@PropertySource(value = {"classpath:appSettings.yaml"}, factory = RuYiAdminYamlPropertySourceFactory.class)
@Data
public class SM4Config {
    //模式，CBC或者ECB模式
    private String Model;
    //密钥
    private String SecretKey;
    //偏移量
    private String IV;
}
