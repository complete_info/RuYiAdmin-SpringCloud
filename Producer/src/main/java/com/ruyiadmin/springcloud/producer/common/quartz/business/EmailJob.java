package com.ruyiadmin.springcloud.producer.common.quartz.business;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

/**
 * <p>
 * 系统邮件定时任务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-04
 */
@Slf4j
public class EmailJob implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        log.info("mail job executed");
    }

}
