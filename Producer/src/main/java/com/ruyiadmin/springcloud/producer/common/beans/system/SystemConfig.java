package com.ruyiadmin.springcloud.producer.common.beans.system;

import com.ruyiadmin.springcloud.producer.common.factories.RuYiAdminYamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 系统配置类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-18
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "systemconfig")
@PropertySource(value = {"classpath:appSettings.yaml"}, factory = RuYiAdminYamlPropertySourceFactory.class)
@Data
public class SystemConfig {
    // Pem格式Rsa公钥
    private String RsaPublicKey;
    // Pem格式Rsa私钥
    private String RsaPrivateKey;
    // 白名单
    private String WhiteList;
    // Header配置
    private String HeaderConfig;
    // 机构根目录编号
    private String OrgRoot;
    // 默认密码
    private String DefaultPassword;
    // AesKey
    private String AesKey;
    // 登录上限
    private int LogonCountLimit;
    // TokenKey
    private String TokenKey;
    // 检测Token开关
    private boolean CheckToken;
    // token有效时间（分钟）
    private int UserTokenExpiration;
    // 检测JwtToken开关
    private boolean CheckJwtToken;
    // 生产环境是否支持SwaggerUI
    private boolean SupportSwagger;
}
