package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruyiadmin.springcloud.producer.common.exceptions.RuYiAdminCustomException;
import com.ruyiadmin.springcloud.producer.domain.dto.system.ImportConfigDTO;
import com.ruyiadmin.springcloud.producer.domain.dto.system.ImportConfigDetailDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysImportConfig;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysImportConfigDetail;
import com.ruyiadmin.springcloud.producer.repository.system.ISysImportConfigDetailRepository;
import com.ruyiadmin.springcloud.producer.repository.system.ISysImportConfigRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysImportConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 导入配置主表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
@RequiredArgsConstructor
public class SysImportConfigServiceImpl extends ServiceImpl<ISysImportConfigRepository,
        SysImportConfig> implements ISysImportConfigService {

    //region 实现类私有属性

    private final ModelMapper modelMapper;
    private final ISysImportConfigRepository importConfigRepository;
    private final ISysImportConfigDetailRepository detailRepository;

    //endregion

    //region 获取导入配置

    /**
     * 获取导入配置
     *
     * @param configName 配置名称
     * @return 导入配置
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ImportConfigDTO getImportConfig(String configName) {
        QueryWrapper<SysImportConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CONFIG_NAME", configName);
        SysImportConfig importConfig = this.importConfigRepository.selectOne(queryWrapper);
        if (importConfig == null) {
            throw new RuYiAdminCustomException("can not find import config");
        }

        QueryWrapper<SysImportConfigDetail> query = new QueryWrapper<>();
        query.eq("PARENT_ID", importConfig.getId());
        List<SysImportConfigDetail> configDetails = this.detailRepository.selectList(query);

        ImportConfigDTO configDTO = this.modelMapper.map(importConfig, ImportConfigDTO.class);

        if (configDetails.size() > 0) {
            configDTO.setChildren(new ArrayList<>());
            configDetails.forEach(item -> {
                ImportConfigDetailDTO configDetailDTO = this.modelMapper.map(item, ImportConfigDetailDTO.class);
                configDTO.getChildren().add(configDetailDTO);
            });
        }

        return configDTO;
    }

    //endregion

}
