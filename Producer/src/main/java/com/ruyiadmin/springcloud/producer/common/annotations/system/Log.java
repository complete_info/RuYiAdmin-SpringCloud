package com.ruyiadmin.springcloud.producer.common.annotations.system;

import com.ruyiadmin.springcloud.producer.common.core.business.enums.OperationType;
import org.apache.commons.lang3.StringUtils;

import java.lang.annotation.*;

/**
 * <p>
 * 操作日志注解
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-20
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    //操作类型
    OperationType OperationType() default OperationType.QueryList;

    //行为描述信息
    String Description() default StringUtils.EMPTY;

}
