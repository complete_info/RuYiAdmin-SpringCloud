package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysLanguage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统语言 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysLanguageService extends IService<SysLanguage> {

    /**
     * 加载系统多语缓存
     */
    void loadSysLanguageCache();

    /**
     * 清理系统多语缓存
     */
    void clearSysLanguageCache();

}
