package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色菜单关系表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

    /**
     * 加载角色与菜单缓存
     */
    void loadSysRoleMenuCache();

    /**
     * 清理角色与菜单缓存
     */
    void clearSysRoleMenuCache();

}
