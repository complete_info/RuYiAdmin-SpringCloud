package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.alibaba.fastjson.JSON;
import com.ruyiadmin.springcloud.producer.common.beans.system.SystemCacheConfig;
import com.ruyiadmin.springcloud.producer.common.components.core.RuYiRedisComponent;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysLanguage;
import com.ruyiadmin.springcloud.producer.repository.system.ISysLanguageRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysLanguageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 系统语言 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SysLanguageServiceImpl extends ServiceImpl<ISysLanguageRepository, SysLanguage>
        implements ISysLanguageService {

    //region 实现类私有属性

    private final RuYiRedisComponent redisUtils;
    private final SystemCacheConfig systemCacheConfig;

    //endregion

    //region 加载系统多语缓存

    /**
     * 加载系统多语缓存
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void loadSysLanguageCache() {
        List<SysLanguage> languages = this.list();
        this.redisUtils.set(systemCacheConfig.getLanguageCacheName(), JSON.toJSONString(languages));
        log.info("RuYiAdmin sys languages cache loaded");
    }

    //endregion

    //region 清理系统多语缓存

    /**
     * 清理系统多语缓存
     */
    @Override
    public void clearSysLanguageCache() {
        this.redisUtils.del(systemCacheConfig.getLanguageCacheName());
        log.info("RuYiAdmin sys languages cache cleared");
    }

    //endregion

}
