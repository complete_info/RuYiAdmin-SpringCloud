package com.ruyiadmin.springcloud.producer.domain.dto.system;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * 系统用户DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-07-17
 */
@Data
public class SysUserDTO {

    @TableId("ID")
    @ApiModelProperty("主键")
    private String id;

    @TableField("LOGON_NAME")
    @ApiModelProperty("登录账号")
    private String logonName;

    @TableField("DISPLAY_NAME")
    @ApiModelProperty("用户名")
    private String displayName;

    @TableField("PASSWORD")
    @ApiModelProperty("用户密码")
    private String password;

    @TableField("TELEPHONE")
    @ApiModelProperty("座机号")
    private String telephone;

    @TableField("MOBILEPHONE")
    @ApiModelProperty("手机号")
    private String mobilephone;

    @TableField("EMAIL")
    @ApiModelProperty("电子邮件")
    private String email;

    @TableField("IS_SUPPER_ADMIN")
    @ApiModelProperty("是否超级用户，0：否，1：是")
    private int isSupperAdmin;

    @TableField("SERIAL_NUMBER")
    @ApiModelProperty("序号")
    private int serialNumber;

    @TableField("IS_ENABLED")
    @ApiModelProperty("是否启用")
    private int isEnabled;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    private String remark;

    @ApiModelProperty("是否删除标志位")
    @TableField("ISDEL")
    @TableLogic //逻辑删除
    private int isdel;

    @ApiModelProperty("创建人")
    @TableField("CREATOR")
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER")
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @Version    // 乐观锁注解
    private String versionId;

    @ApiModelProperty("性别，男：0，女：1，第三性别：2")
    @TableField("SEX")
    @NotNull
    private Integer sex;

    @ApiModelProperty("盐")
    @TableField("SALT")
    @NotNull
    private String salt;

    @ApiModelProperty("预留字段1")
    @TableField("EXTEND1")
    @Max(36)
    private String extend1;

    @ApiModelProperty("预留字段2")
    @TableField("EXTEND2")
    @Max(64)
    private String extend2;

    @ApiModelProperty("预留字段3")
    @TableField("EXTEND3")
    @Max(128)
    private String extend3;

    @ApiModelProperty("预留字段4")
    @TableField("EXTEND4")
    @Max(256)
    private String extend4;

    @ApiModelProperty("预留字段5")
    @TableField("EXTEND5")
    @Max(512)
    private String extend5;

    @TableField("ORG_ID")
    @ApiModelProperty("机构编号")
    private String orgId;

    @TableField("ORG_NAME")
    @ApiModelProperty("机构名称")
    private String orgName;

    @TableField(exist = false)
    @ApiModelProperty("token")
    private String token;

    @TableField(exist = false)
    @ApiModelProperty("token有效时间，单位：秒")
    private int tokenExpiration;
}
