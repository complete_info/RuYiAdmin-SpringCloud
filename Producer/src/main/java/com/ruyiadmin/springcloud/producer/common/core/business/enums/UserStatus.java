package com.ruyiadmin.springcloud.producer.common.core.business.enums;

/**
 * <p>
 * 用户状态枚举
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-31
 */
public enum UserStatus {
    //禁用
    Disabled,
    //启用
    Enabled
}
