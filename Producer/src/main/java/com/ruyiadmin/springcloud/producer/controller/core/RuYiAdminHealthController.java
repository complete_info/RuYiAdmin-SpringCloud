package com.ruyiadmin.springcloud.producer.controller.core;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.ruyiadmin.springcloud.producer.common.annotations.core.AllowAnonymous;
import com.ruyiadmin.springcloud.producer.common.utils.system.RuYiSentinelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统健康检查服务 前端控制器
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-17
 */
@RestController
@RequestMapping("/RuYiAdminHealth")
@Api(tags = "系统健康检查服务")
public class RuYiAdminHealthController {

    //region 系统健康检查

    @GetMapping("/Get")
    @ApiOperation(value = "系统健康检查")
    @AllowAnonymous
    public String get() throws BlockException {
        RuYiSentinelUtil.tryAcquire("HealthCheck");
        return "ok";
    }

    //endregion

}
