package com.ruyiadmin.springcloud.producer.common.configurations.system;

import com.ruyiadmin.springcloud.producer.common.interceptors.core.RuYiAdminAuthenticationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <p>
 * RuYiAdmin拦截器配置
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-16
 */
@Configuration
public class RuYiAdminInterceptorConfiguration implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //默认拦截所有路径
        registry.addInterceptor(authenticationInterceptor())
                .addPathPatterns("/**");
    }

    @Bean
    public RuYiAdminAuthenticationInterceptor authenticationInterceptor() {
        return new RuYiAdminAuthenticationInterceptor();
    }

}
