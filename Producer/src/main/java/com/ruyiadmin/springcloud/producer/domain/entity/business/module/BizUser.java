package com.ruyiadmin.springcloud.producer.domain.entity.business.module;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.ruyiadmin.springcloud.producer.domain.entity.base.RuYiAdminBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 模块用户模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Getter
@Setter
@TableName("biz_user")
@ApiModel(value = "BizUser对象", description = "模块用户表")
@NoArgsConstructor
@AllArgsConstructor
public class BizUser extends RuYiAdminBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("用户编号")
    @TableId("ID")
    @NotNull
    private String id;

    @ApiModelProperty("用户登录账号")
    @TableField("USER_LOGON_NAME")
    @NotNull
    @Max(128)
    private String userLogonName;

    @ApiModelProperty("用户名称")
    @TableField("USER_DISPLAY_NAME")
    @NotNull
    @Max(128)
    private String userDisplayName;

    @ApiModelProperty("用户密码")
    @TableField("USER_PASSWORD")
    @NotNull
    @Max(512)
    private String userPassword;

    @ApiModelProperty("座机")
    @TableField("TELEPHONE")
    @Max(45)
    private String telephone;

    @ApiModelProperty("手机")
    @TableField("MOBILEPHONE")
    @Max(45)
    private String mobilephone;

    @ApiModelProperty("邮箱")
    @TableField("EMAIL")
    @Max(45)
    private String email;

    @ApiModelProperty("是否启用，0：禁用，1：启用")
    @TableField("IS_ENABLED")
    private Integer isEnabled;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @TableLogic
    @NotNull
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

    @ApiModelProperty("性别，男：0，女：1，第三性别：2")
    @TableField("SEX")
    @NotNull
    private Integer sex;

    @ApiModelProperty("盐")
    @TableField("SALT")
    @NotNull
    private String salt;

    @ApiModelProperty("预留字段1")
    @TableField("EXTEND1")
    @Max(36)
    private String extend1;

    @ApiModelProperty("预留字段2")
    @TableField("EXTEND2")
    @Max(64)
    private String extend2;

    @ApiModelProperty("预留字段3")
    @TableField("EXTEND3")
    @Max(128)
    private String extend3;

    @ApiModelProperty("预留字段4")
    @TableField("EXTEND4")
    @Max(256)
    private String extend4;

    @ApiModelProperty("预留字段5")
    @TableField("EXTEND5")
    @Max(512)
    private String extend5;

}
