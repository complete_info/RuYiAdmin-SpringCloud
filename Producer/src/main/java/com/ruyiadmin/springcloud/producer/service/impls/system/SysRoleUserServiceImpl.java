package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.alibaba.fastjson.JSON;
import com.ruyiadmin.springcloud.producer.common.beans.system.SystemCacheConfig;
import com.ruyiadmin.springcloud.producer.common.components.core.RuYiRedisComponent;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysRoleUser;
import com.ruyiadmin.springcloud.producer.repository.system.ISysRoleUserRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysRoleUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 角色用户关系表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SysRoleUserServiceImpl extends ServiceImpl<ISysRoleUserRepository, SysRoleUser>
        implements ISysRoleUserService {

    //region 实现类私有属性

    private final RuYiRedisComponent redisUtils;
    private final SystemCacheConfig systemCacheConfig;

    //endregion

    //region 加载角色与用户缓存

    /**
     * 加载角色与用户缓存
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void loadSysRoleUserCache() {
        List<SysRoleUser> list = this.list();
        this.redisUtils.set(systemCacheConfig.getRoleAndUserCacheName(), JSON.toJSONString(list));
        log.info("RuYiAdmin sys roles and users cache loaded");
    }

    //endregion

    //region 清理角色与用户缓存

    /**
     * 清理角色与用户缓存
     */
    @Override
    public void clearSysRoleUserCache() {
        this.redisUtils.del(systemCacheConfig.getRoleAndUserCacheName());
        log.info("RuYiAdmin sys roles and users cache cleared");
    }

    //endregion

}
