package com.ruyiadmin.springcloud.producer.common.beans.system;

import com.ruyiadmin.springcloud.producer.common.factories.RuYiAdminYamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 系统缓存配置类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-14
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "jwtsettings")
@PropertySource(value = {"classpath:appSettings.yaml"}, factory = RuYiAdminYamlPropertySourceFactory.class)
@Data
public class JwtSettings {
    //其否启用
    private boolean IsEnabled;
    // 订阅者
    private String Audience;
    // 签发者
    private String Issuer;
    // 密钥
    private String SecurityKey;
    // 缺省用户
    private String DefaultUser;
    // 缺省密码
    private String DefaultPassword;
    // 盐有效时间（秒）
    private int SaltExpiration;
    // token有效时间（分钟）
    private int TokenExpiration;
}
