package com.ruyiadmin.springcloud.producer.domain.entity.system;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.ruyiadmin.springcloud.producer.domain.entity.base.RuYiAdminBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 收件人模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
@TableName("sys_addressee")
@ApiModel(value = "SysAddressee对象", description = "收件人表")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysAddressee extends RuYiAdminBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId("ID")
    @NotNull
    private String id;

    @ApiModelProperty("业务编号")
    @TableField("BUSINESS_ID")
    @NotNull
    private String businessId;

    @ApiModelProperty("用户编号")
    @TableField("USER_ID")
    @NotNull
    private String userId;

    @ApiModelProperty("状态，0：未读，1：已读")
    @TableField("STATUS")
    @NotNull
    private Integer status;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @TableLogic
    @NotNull
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

}
