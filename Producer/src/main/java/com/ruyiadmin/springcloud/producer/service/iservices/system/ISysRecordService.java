package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysRecord;

/**
 * <p>
 * 系统记录表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-09-04
 */
public interface ISysRecordService extends IService<SysRecord> {

}
