package com.ruyiadmin.springcloud.producer;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.spring.web.SpringfoxWebMvcConfiguration;

@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.ruyiadmin.springcloud.producer.repository")
@EnableTransactionManagement
@EnableAsync
@EnableCaching
@EnableJms
@NacosPropertySource(dataId = "RuYiAdmin-SpringCloud-Nacos-Config", autoRefreshed = true)
@ConditionalOnClass(SpringfoxWebMvcConfiguration.class)
public class ProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProducerApplication.class, args);
    }

}
