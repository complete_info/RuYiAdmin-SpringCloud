package com.ruyiadmin.springcloud.producer.common.core.system.enums;

/**
 * <p>
 * 查询方法枚举
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-17
 */
public enum QueryMethod {
    Equal,
    Like,
    LessThan,
    LessThanOrEqual,
    GreaterThan,
    GreaterThanOrEqual,
    BetweenAnd,
    Include,
    OrLike,
    NotEqual
}
