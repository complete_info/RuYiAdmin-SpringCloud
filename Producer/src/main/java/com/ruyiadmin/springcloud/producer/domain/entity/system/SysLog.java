package com.ruyiadmin.springcloud.producer.domain.entity.system;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.ruyiadmin.springcloud.producer.domain.entity.base.RuYiAdminBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 审计日志模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Getter
@Setter
@TableName("sys_log")
@ApiModel(value = "SysLog对象", description = "审计日志表")
@Document("SysLog")
@NoArgsConstructor
@AllArgsConstructor
public class SysLog extends RuYiAdminBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId("ID")
    @Id
    @NotNull
    private String id;

    @ApiModelProperty("用户编号")
    @TableField("USER_ID")
    @NotNull
    private String userId;

    @ApiModelProperty("用户姓名")
    @TableField("USER_NAME")
    @NotNull
    @Max(128)
    private String userName;

    @ApiModelProperty("机构编号")
    @TableField("ORG_ID")
    @NotNull
    private String orgId;

    @ApiModelProperty("机构名称")
    @TableField("ORG_NAME")
    @NotNull
    @Max(128)
    private String orgName;

    @ApiModelProperty("使用系统")
    @TableField("`SYSTEM`")
    private String system;

    @ApiModelProperty("浏览器")
    @TableField("BROWSER")
    private String browser;

    @ApiModelProperty("IP地址")
    @TableField("IP")
    private String ip;

    @ApiModelProperty("操作类型，0：查询列表，1：查询记录，2：新增，3：编辑，4：逻辑删除，5：物理删除，6：上传，7：下载，8：导入，" +
            "9：导出，10：菜单授权，11：用户授权，12：打印，13：登录，14：登出，15：强制登出，16：更新密码，17：添加计划任务，" +
            "18：暂停计划任务，19：启动计划任务")
    @TableField("OPERATION_TYPE")
    @NotNull
    private Integer operationType;

    @ApiModelProperty("请求方法")
    @TableField("REQUEST_METHOD")
    @NotNull
    private String requestMethod;

    @ApiModelProperty("请求路径")
    @TableField("REQUEST_URL")
    @NotNull
    private String requestUrl;

    @ApiModelProperty("请求参数")
    @TableField("PARAMS")
    private String params;

    @ApiModelProperty("返回结果")
    @TableField("RESULT")
    private String result;

    @ApiModelProperty("旧值")
    @TableField("OLD_VALUE")
    private String oldValue;

    @ApiModelProperty("新值")
    @TableField("NEW_VALUE")
    private String newValue;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @TableLogic
    @NotNull
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

}
