package com.ruyiadmin.springcloud.producer.common.core.system.entities;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.base.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * <p>
 * RuYiAdmin统一执行结果
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-14
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ActionResult extends BaseDomain {
    //Http状态码
    private int httpStatusCode;
    //返回消息
    private String message;
    //返回值
    private Object object;

    public static ActionResult success(Object object) {
        return new ActionResult(HttpStatus.OK.value(), "OK", object);
    }

    public static ActionResult ok() {
        return new ActionResult(HttpStatus.OK.value(), "OK", "OK");
    }

    public static ActionResult error() {
        return new ActionResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), "ERROR", null);
    }

    public static ActionResult error(String message) {
        return new ActionResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, null);
    }

    public static ActionResult badRequest() {
        return new ActionResult(HttpStatus.BAD_REQUEST.value(), "BAD_REQUEST", null);
    }

    public static ActionResult badRequest(String message) {
        return new ActionResult(HttpStatus.BAD_REQUEST.value(), message, null);
    }

    public static ActionResult unauthorized() {
        return new ActionResult(HttpStatus.UNAUTHORIZED.value(), "UNAUTHORIZED", null);
    }

    public static ActionResult unauthorized(String message) {
        return new ActionResult(HttpStatus.UNAUTHORIZED.value(), message, null);
    }

    public static ActionResult forbidden() {
        return new ActionResult(HttpStatus.FORBIDDEN.value(), "FORBIDDEN", null);
    }

    public static ActionResult forbidden(String message) {
        return new ActionResult(HttpStatus.FORBIDDEN.value(), message, null);
    }

    public static ActionResult notFound() {
        return new ActionResult(HttpStatus.NOT_FOUND.value(), "NOT_FOUND", null);
    }

    public static ActionResult notFound(String message) {
        return new ActionResult(HttpStatus.NOT_FOUND.value(), message, null);
    }

    public static ActionResult noContent() {
        return new ActionResult(HttpStatus.NO_CONTENT.value(), "NO_CONTENT", null);
    }

    public static ActionResult noContent(String message) {
        return new ActionResult(HttpStatus.NO_CONTENT.value(), message, null);
    }

    public static ActionResult tooManyRequests() {
        return new ActionResult(HttpStatus.TOO_MANY_REQUESTS.value(), "TOO_MANY_REQUESTS", null);
    }

    public static ActionResult tooManyRequests(String message) {
        return new ActionResult(HttpStatus.TOO_MANY_REQUESTS.value(), message, null);
    }
}
