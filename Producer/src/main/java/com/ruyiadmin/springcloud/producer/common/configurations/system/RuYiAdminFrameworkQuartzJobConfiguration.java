package com.ruyiadmin.springcloud.producer.common.configurations.system;

import com.ruyiadmin.springcloud.producer.common.beans.system.QuartzConfig;
import com.ruyiadmin.springcloud.producer.common.quartz.core.RuYiAdminFrameworkQuartzJob;
import lombok.RequiredArgsConstructor;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 * RuYiAdmin QuartzJob配置
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-16
 */
@Configuration
@RequiredArgsConstructor
public class RuYiAdminFrameworkQuartzJobConfiguration {

    private final QuartzConfig quartzConfig;

    @Bean
    public JobDetail frameworkQuartzJobDetail(){
        return JobBuilder.newJob(RuYiAdminFrameworkQuartzJob.class)//业务类
                .withIdentity("RuYiAdminFrameworkQuartzJob")//JobId
                //每个JobDetail内都有一个Map，包含了关联到这个Job的数据，在Job类中可以通过context获取
                .usingJobData("msg", "RuYiAdmin framework quartz job executed!")//关联键值对
                .storeDurably()//常态运行
                .build();
    }

    @Bean
    public Trigger frameworkQuartzJobTrigger() {
        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.
                cronSchedule(this.quartzConfig.getRuYiAdminFrameworkJob());
        return TriggerBuilder.newTrigger()
                .forJob(frameworkQuartzJobDetail())//关联上述JobDetail
                .withIdentity("RuYiAdminFrameworkQuartzJob-trigger")//TriggerName
                .withSchedule(cronScheduleBuilder)
                .build();
    }

}
