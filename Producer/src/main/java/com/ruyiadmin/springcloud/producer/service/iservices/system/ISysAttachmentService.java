package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.ActionResult;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysAttachment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统附件表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
public interface ISysAttachmentService extends IService<SysAttachment> {

    /**
     * 查询系统文件统计信息
     *
     * @return ActionResult
     */
    ActionResult querySysFileStatisticalInfo();

}
