package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.domain.dto.system.ImportConfigDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysImportConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 导入配置主表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysImportConfigService extends IService<SysImportConfig> {
    /**
     * 获取导入配置
     *
     * @param configName 配置名称
     * @return 导入配置
     */
    ImportConfigDTO getImportConfig(String configName);
}
