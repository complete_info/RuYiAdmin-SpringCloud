package com.ruyiadmin.springcloud.producer.domain.entity.system;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.ruyiadmin.springcloud.producer.domain.entity.base.RuYiAdminBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 计划任务模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Getter
@Setter
@TableName("sys_schedule_job")
@ApiModel(value = "SysScheduleJob对象", description = "计划任务表")
public class SysScheduleJob extends RuYiAdminBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId("ID")
    @NotNull
    private String id;

    @ApiModelProperty("任务名称")
    @TableField("JOB_NAME")
    @NotNull
    @Max(128)
    private String jobName;

    @ApiModelProperty("任务描述")
    @TableField("JOB_DESCRIPTION")
    @Max(512)
    private String jobDescription;

    @ApiModelProperty("命名空间")
    @TableField("NAMESPACE")
    @NotNull
    @Max(512)
    private String namespace;

    @ApiModelProperty("实现类")
    @TableField("JOB_IMPLEMENT")
    @NotNull
    @Max(128)
    private String jobImplement;

    @ApiModelProperty("Cron表达式")
    @TableField("CRON_EXPRESSION")
    @NotNull
    @Max(128)
    private String cronExpression;

    @ApiModelProperty("开始时间")
    @TableField("START_TIME")
    private Date startTime;

    @ApiModelProperty("结束时间")
    @TableField("END_TIME")
    private Date endTime;

    @ApiModelProperty("任务状态，0：已启用，1：运行中，2：执行中，3：执行完成，4：任务计划中，5：已停止")
    @TableField("JOB_STATUS")
    @NotNull
    private Integer jobStatus;

    @ApiModelProperty("集群编号")
    @TableField("GROUP_ID")
    private Integer groupId;

    @ApiModelProperty("序号")
    @TableField("SERIAL_NUMBER")
    private Integer serialNumber;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @TableLogic
    @NotNull
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

}
