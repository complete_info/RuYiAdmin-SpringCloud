package com.ruyiadmin.springcloud.producer.service.iservices.business.module;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryCondition;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.business.module.BizUserModuleDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.business.module.BizUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 模块用户表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface IBizUserService extends IService<BizUser> {

    /**
     * <p>
     * 查询离态用户列表
     * </p>
     *
     * @param queryCondition 查询条件
     * @return 查询结果
     */
    QueryResult<BizUserModuleDTO> queryUserNonModule(QueryCondition queryCondition);

    /**
     * <p>
     * 查询所有业务用户
     * </p>
     *
     * @return 业务用户集合
     */
    List<BizUser> queryAllBizUsers();

}
