package com.ruyiadmin.springcloud.producer.domain.entity.system;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.ruyiadmin.springcloud.producer.domain.entity.base.RuYiAdminBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 系统语言模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@TableName("sys_language")
@ApiModel(value = "SysLanguage对象", description = "系统语言表")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysLanguage extends RuYiAdminBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId("ID")
    @NotNull
    private String id;

    @ApiModelProperty("名称")
    @TableField("LANGUAGE_NAME")
    @NotNull
    @Max(45)
    private String languageName;

    @ApiModelProperty("序号")
    @TableField("ORDER_NUMBER")
    private Integer orderNumber;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @TableLogic
    @NotNull
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

    @ApiModelProperty("预留字段1")
    @TableField("EXTEND1")
    @Max(36)
    private String extend1;

    @ApiModelProperty("预留字段2")
    @TableField("EXTEND2")
    @Max(64)
    private String extend2;

    @ApiModelProperty("预留字段3")
    @TableField("EXTEND3")
    @Max(128)
    private String extend3;

    @ApiModelProperty("预留字段4")
    @TableField("EXTEND4")
    @Max(256)
    private String extend4;

    @ApiModelProperty("预留字段5")
    @TableField("EXTEND5")
    @Max(512)
    private String extend5;

}
