package com.ruyiadmin.springcloud.producer.domain.dto.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysAnnouncement;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 系统通知DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-08-05
 */
@Data
public class SysNotificationDTO extends SysAnnouncement {
    @ApiModelProperty("阅读状态，0：未读，1：已读")
    @TableField("READED")
    private int readed;
}
