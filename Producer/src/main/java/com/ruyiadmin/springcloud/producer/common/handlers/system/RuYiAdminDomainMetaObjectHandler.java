package com.ruyiadmin.springcloud.producer.common.handlers.system;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.ruyiadmin.springcloud.producer.common.constants.system.Guid;
import com.ruyiadmin.springcloud.producer.common.core.business.enums.YesNo;
import com.ruyiadmin.springcloud.producer.common.components.core.RuYiSessionContext;
import com.ruyiadmin.springcloud.producer.common.utils.core.RuYiCommonUtil;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysUserDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysLog;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * <p>
 * RuYiAdmin领域数据自动填充器
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-22
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class RuYiAdminDomainMetaObjectHandler implements MetaObjectHandler {

    private final RuYiSessionContext sessionContext;

    //region 插入时的填充

    @Override
    public void insertFill(MetaObject metaObject) {
        if (metaObject.getOriginalObject().getClass() == SysLog.class) {
            return;
        }
        try {
            SysUserDTO user = sessionContext.getCurrentUserInfo();
            if (user != null) {
                this.setFieldValByName("creator", user.getId(), metaObject);
                this.setFieldValByName("modifier", user.getId(), metaObject);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            ex.printStackTrace();
        } finally {
            log.info("insertFill");
            if (metaObject.getValue("id") == null) {
                this.setFieldValByName("id", UUID.randomUUID().toString(), metaObject);
            } else if (metaObject.getValue("id").toString().equals(Guid.Empty)
                    || !RuYiCommonUtil.isUuid(metaObject.getValue("id").toString())) {
                this.setFieldValByName("id", UUID.randomUUID().toString(), metaObject);
            }
            this.setFieldValByName("isdel", YesNo.NO.ordinal(), metaObject);
            this.setFieldValByName("createTime", LocalDateTimeUtil.now(), metaObject);
            this.setFieldValByName("modifyTime", LocalDateTimeUtil.now(), metaObject);
            this.setFieldValByName("versionId", UUID.randomUUID().toString(), metaObject);
        }
    }

    //endregion

    //region  修改时的填充

    @Override
    public void updateFill(MetaObject metaObject) {
        if (metaObject.getOriginalObject().getClass() == SysLog.class) {
            return;
        }
        try {
            SysUserDTO user = sessionContext.getCurrentUserInfo();
            if (user != null) {
                this.setFieldValByName("modifier", user.getId(), metaObject);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            ex.printStackTrace();
        } finally {
            log.info("updateFill");
            this.setFieldValByName("modifyTime", LocalDateTimeUtil.now(), metaObject);
            this.setFieldValByName("versionId", UUID.randomUUID().toString(), metaObject);
        }
    }

    //endregion

}
