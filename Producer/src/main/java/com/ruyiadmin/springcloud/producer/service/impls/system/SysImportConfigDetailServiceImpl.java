package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysImportConfigDetail;
import com.ruyiadmin.springcloud.producer.repository.system.ISysImportConfigDetailRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysImportConfigDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 导入配置子表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
public class SysImportConfigDetailServiceImpl extends ServiceImpl<ISysImportConfigDetailRepository,
        SysImportConfigDetail> implements ISysImportConfigDetailService {

}
