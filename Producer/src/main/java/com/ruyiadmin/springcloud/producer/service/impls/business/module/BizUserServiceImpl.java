package com.ruyiadmin.springcloud.producer.service.impls.business.module;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryCondition;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.business.module.BizUserModuleDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.business.module.BizUser;
import com.ruyiadmin.springcloud.producer.repository.business.module.IBizUserRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.business.module.IBizUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 模块用户表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
@RequiredArgsConstructor
public class BizUserServiceImpl extends ServiceImpl<IBizUserRepository, BizUser> implements IBizUserService {

    //region 实现类私有属性

    private final IBizUserRepository bizUserRepository;

    //endregion

    //region 查询离态用户列表

    /**
     * <p>
     * 查询离态用户列表
     * </p>
     *
     * @param queryCondition 查询条件
     * @return 查询结果
     */
    @Override
    public QueryResult<BizUserModuleDTO> queryUserNonModule(QueryCondition queryCondition) {
        QueryWrapper<BizUserModuleDTO> wrapper = new QueryWrapper<>();
        queryCondition.getQueryWrapper(wrapper);

        Page<BizUserModuleDTO> page = new Page<>(queryCondition.getPageIndex(), queryCondition.getPageSize());
        IPage<BizUserModuleDTO> records = this.bizUserRepository.queryUserNonModule(page, wrapper);

        return QueryResult.success(records.getTotal(), records.getRecords());
    }

    //endregion

    //region 查询所有业务用户

    /**
     * <p>
     * 查询所有业务用户
     * </p>
     *
     * @return 业务用户集合
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public List<BizUser> queryAllBizUsers() {
        return this.bizUserRepository.queryAllBizUsers();
    }

    //endregion

}
