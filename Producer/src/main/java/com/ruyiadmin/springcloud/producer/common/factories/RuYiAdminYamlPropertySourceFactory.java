package com.ruyiadmin.springcloud.producer.common.factories;

import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.support.DefaultPropertySourceFactory;
import org.springframework.core.io.support.EncodedResource;

import java.io.IOException;
import java.util.List;

/**
 * 配置文件工厂类
 *
 * @author RuYiAdmin
 * @since 2022-06-28
 */
public class RuYiAdminYamlPropertySourceFactory extends DefaultPropertySourceFactory {
    @Override
    public PropertySource<?> createPropertySource(String name, EncodedResource resource) throws IOException {
        if (resource == null) {
            return super.createPropertySource(name, resource);
        }
        List<PropertySource<?>> sources = new YamlPropertySourceLoader().load(
                resource.getResource().getFilename(),
                resource.getResource()
        );
        return sources.get(0);
    }
}

