package com.ruyiadmin.springcloud.producer.domain.dto.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysMenu;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 系统菜单DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-07-17
 */
@Data
public class SysMenuDTO extends SysMenu {

    @ApiModelProperty("系统菜单子集")
    @TableField(exist = false)
    private List<SysMenuDTO> children;

    @ApiModelProperty("英文菜单名称")
    @TableField(exist = false)
    private String menuNameEn;

    @ApiModelProperty("俄文菜单名称")
    @TableField(exist = false)
    private String MenuNameRu;

}
