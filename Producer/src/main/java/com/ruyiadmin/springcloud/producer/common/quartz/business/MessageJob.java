package com.ruyiadmin.springcloud.producer.common.quartz.business;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

/**
 * <p>
 * 系统短信定时任务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-04
 */
@Slf4j
public class MessageJob implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        log.info("message job executed");
    }

}
