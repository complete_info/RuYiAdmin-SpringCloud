package com.ruyiadmin.springcloud.producer.domain.dto.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysAnnouncement;
import lombok.Data;

/**
 * 系统公告DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-08-05
 */
@Data
public class SysAnnouncementDTO extends SysAnnouncement {
    //收件人列表
    private String addressee;
    //附件编号
    private String attachmentIds;
    //是否发送邮件
    private boolean sendMail;
}
