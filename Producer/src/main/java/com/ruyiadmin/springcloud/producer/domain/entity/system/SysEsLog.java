package com.ruyiadmin.springcloud.producer.domain.entity.system;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

/**
 * <p>
 * ES审计日志模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "ruyiadmin-springboot")
public class SysEsLog {

    @Id
    @Field(store = true, type = FieldType.Keyword)
    private String id;

    @Field(store = true, type = FieldType.Keyword)
    private String userId;

    @Field(store = true, type = FieldType.Keyword)
    private String userName;

    @Field(store = true, type = FieldType.Keyword)
    private String orgId;

    @Field(store = true, type = FieldType.Keyword)
    private String orgName;

    @Field(store = true, type = FieldType.Text)//, analyzer = "ik_smart"
    private String system;

    @Field(store = true, type = FieldType.Text)//, analyzer = "ik_smart"
    private String browser;

    @Field(store = true, type = FieldType.Ip)
    private String ip;

    @Field(store = true, type = FieldType.Integer)
    private int operationType;

    @Field(store = true, type = FieldType.Text)//, analyzer = "ik_smart"
    private String requestMethod;

    @Field(store = true, type = FieldType.Text)//, analyzer = "ik_smart"
    private String requestUrl;

    @Field(store = true, type = FieldType.Text)//, analyzer = "ik_smart"
    private String params;

    @Field(store = true, type = FieldType.Text)//, analyzer = "ik_smart"
    private String result;

    @Field(store = true, type = FieldType.Text)//, analyzer = "ik_smart"
    private String oldValue;

    @Field(store = true, type = FieldType.Text)//, analyzer = "ik_smart"
    private String newValue;

    @Field(store = true, type = FieldType.Text)//, analyzer = "ik_smart"
    private String remark;

    @Field(store = true, type = FieldType.Integer)
    private int isdel;

    @Field(store = true, type = FieldType.Keyword)
    private String creator;

    @Field(store = true,  type = FieldType.Date, format = DateFormat.basic_date_time)
    private Date createTime;

    @Field(store = true, type = FieldType.Keyword)
    private String modifier;

    @Field(store = true,  type = FieldType.Date, format = DateFormat.basic_date_time)
    private Date modifyTime;

    @Field(store = true, type = FieldType.Keyword)
    private String versionId;

}
