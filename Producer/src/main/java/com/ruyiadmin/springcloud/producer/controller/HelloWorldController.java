package com.ruyiadmin.springcloud.producer.controller;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.ruyiadmin.springcloud.producer.common.annotations.core.AllowAnonymous;
import com.ruyiadmin.springcloud.producer.common.utils.system.RuYiSentinelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/helloWorld")
@Api(tags = "系统HelloWorld测试服务")
public class HelloWorldController {
    @AllowAnonymous
    @GetMapping("/sayHello")
    @ApiOperation(value = "SayHello测试方法")
    public String sayHello() throws BlockException {
        RuYiSentinelUtil.tryAcquire("SayHello");
        return "Hello consumer,I`m producer 1!";
    }
}
