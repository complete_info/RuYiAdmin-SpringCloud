package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.ActionResult;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysAddressee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收件人表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
public interface ISysAddresseeService extends IService<SysAddressee> {
    /**
     * 更改通知收件人阅读状态
     *
     * @param notificationId 通知编号
     * @return ActionResult
     * @throws Exception 异常
     */
    ActionResult updateNotificationStatus(String notificationId) throws Exception;
}
