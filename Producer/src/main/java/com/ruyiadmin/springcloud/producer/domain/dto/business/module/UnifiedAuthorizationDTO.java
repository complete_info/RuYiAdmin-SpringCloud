package com.ruyiadmin.springcloud.producer.domain.dto.business.module;

import lombok.Data;

/**
 * <p>
 * 统一认证DTO模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-09-07
 */
@Data
public class UnifiedAuthorizationDTO {
    private String token;
    private String userId;
    private String userPassword;
}
