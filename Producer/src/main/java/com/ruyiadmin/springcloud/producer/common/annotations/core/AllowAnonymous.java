package com.ruyiadmin.springcloud.producer.common.annotations.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * AllowAnonymous注解
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-16
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AllowAnonymous {
    boolean required() default true;
}
