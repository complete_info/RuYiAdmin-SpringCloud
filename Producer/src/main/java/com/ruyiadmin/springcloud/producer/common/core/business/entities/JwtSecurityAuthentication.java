package com.ruyiadmin.springcloud.producer.common.core.business.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 系统Jwt授权验证类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-15
 */
@Data
public class JwtSecurityAuthentication {

    //用户名
    @NotNull(message = "can not be null")
    @NotBlank(message = "can not be null")
    @NotEmpty(message = "can not be null")
    @JsonProperty(value = "UserName")
    private String userName;

    //密码
    @NotNull(message = "can not be null")
    @NotBlank(message = "can not be null")
    @NotEmpty(message = "can not be null")
    @JsonProperty(value = "Password")
    private String password;

    //盐
    @NotNull(message = "can not be null")
    @NotBlank(message = "can not be null")
    @NotEmpty(message = "can not be null")
    @JsonProperty(value = "Salt")
    private String salt;
}
