package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.alibaba.fastjson.JSON;
import com.ruyiadmin.springcloud.producer.common.beans.system.SystemCacheConfig;
import com.ruyiadmin.springcloud.producer.common.components.core.RuYiRedisComponent;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysRoleMenu;
import com.ruyiadmin.springcloud.producer.repository.system.ISysRoleMenuRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 角色菜单关系表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SysRoleMenuServiceImpl extends ServiceImpl<ISysRoleMenuRepository, SysRoleMenu>
        implements ISysRoleMenuService {

    //region 实现类私有属性

    private final RuYiRedisComponent redisUtils;
    private final SystemCacheConfig systemCacheConfig;

    //endregion

    //region 加载角色与菜单缓存

    /**
     * 加载角色与菜单缓存
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void loadSysRoleMenuCache() {
        List<SysRoleMenu> list = this.list();
        this.redisUtils.set(systemCacheConfig.getRoleAndMenuCacheName(), JSON.toJSONString(list));
        log.info("RuYiAdmin sys roles and menus cache loaded");
    }

    //endregion

    //region 清理角色与菜单缓存

    /**
     * 清理角色与菜单缓存
     */
    @Override
    public void clearSysRoleMenuCache() {
        this.redisUtils.del(systemCacheConfig.getRoleAndMenuCacheName());
        log.info("RuYiAdmin sys roles and menus cache cleared");
    }

    //endregion

}
