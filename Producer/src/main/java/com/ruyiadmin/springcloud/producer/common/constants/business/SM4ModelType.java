package com.ruyiadmin.springcloud.producer.common.constants.business;

/**
 * <p>
 * RuYiAdmin SM4模式类型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-11-15
 */
public class SM4ModelType {
    //CBC
    public static final String CBC = "CBC";
    //ECB
    public static final String ECB = "ECB";
}
