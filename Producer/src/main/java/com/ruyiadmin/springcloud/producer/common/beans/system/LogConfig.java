package com.ruyiadmin.springcloud.producer.common.beans.system;

import com.ruyiadmin.springcloud.producer.common.factories.RuYiAdminYamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 系统日志配置类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-02
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "logconfig")
@PropertySource(value = {"classpath:appSettings.yaml"}, factory = RuYiAdminYamlPropertySourceFactory.class)
@Data
public class LogConfig {
    //审计日志分表采集年份
    private int SplitTableYearTake;
    //是否支持MongoDB
    private boolean SupportMongoDB;
    //是否支持Elasticsearch
    private boolean SupportElasticsearch;
    //是否支持Meilisearch
    private boolean SupportMeilisearch;
}
