package com.ruyiadmin.springcloud.producer.common.configurations.core;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 * 系统ModelMapper配置
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-15
 */
@Configuration
public class ModelMapperConfiguration {
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        //Handling Mismatches
        //modelMapper.typeMap(Order.class, OrderDTO.class).addMappings(mapper -> {
        //    mapper.map(src -> src.getBillingAddress().getStreet(),
        //            Destination::setBillingStreet);
        //    mapper.map(src -> src.getBillingAddress().getCity(),
        //            Destination::setBillingCity);
        //});
        return modelMapper;
    }
}
