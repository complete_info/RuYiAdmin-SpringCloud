package com.ruyiadmin.springcloud.producer.common.core.system.entities;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.base.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.List;

/**
 * <p>
 * RuYiAdmin统一查询结果
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-14
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class QueryResult<T> extends BaseDomain {

    //Http状态码
    private int httpStatusCode;
    //返回消息
    private String message;
    //查询数据总数
    private long totalCount;
    //查询记录
    private List<T> list;

    public static <T> QueryResult<T> success(long totalCount, List<T> list) {
        return new QueryResult<>(HttpStatus.OK.value(), "OK", totalCount, list);
    }

    public static <T> QueryResult<T> error() {
        return new QueryResult<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), "ERROR", 0, null);
    }

    public static <T> QueryResult<T> error(String message) {
        return new QueryResult<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, 0, null);
    }

    public static <T> QueryResult<T> badRequest() {
        return new QueryResult<>(HttpStatus.BAD_REQUEST.value(), "BAD_REQUEST", 0, null);
    }

    public static <T> QueryResult<T> badRequest(String message) {
        return new QueryResult<>(HttpStatus.BAD_REQUEST.value(), message, 0, null);
    }

    public static <T> QueryResult<T> unauthorized() {
        return new QueryResult<>(HttpStatus.UNAUTHORIZED.value(), "UNAUTHORIZED", 0, null);
    }

    public static <T> QueryResult<T> unauthorized(String message) {
        return new QueryResult<>(HttpStatus.UNAUTHORIZED.value(), message, 0, null);
    }

    public static <T> QueryResult<T> forbidden() {
        return new QueryResult<>(HttpStatus.FORBIDDEN.value(), "FORBIDDEN", 0, null);
    }

    public static <T> QueryResult<T> forbidden(String message) {
        return new QueryResult<>(HttpStatus.FORBIDDEN.value(), message, 0, null);
    }

    public static <T> QueryResult<T> notFound() {
        return new QueryResult<>(HttpStatus.NOT_FOUND.value(), "NOT_FOUND", 0, null);
    }

    public static <T> QueryResult<T> notFound(String message) {
        return new QueryResult<>(HttpStatus.NOT_FOUND.value(), message, 0, null);
    }

    public static <T> QueryResult<T> noContent() {
        return new QueryResult<>(HttpStatus.NO_CONTENT.value(), "NO_CONTENT", 0, null);
    }

    public static <T> QueryResult<T> noContent(String message) {
        return new QueryResult<>(HttpStatus.NO_CONTENT.value(), message, 0, null);
    }

}
