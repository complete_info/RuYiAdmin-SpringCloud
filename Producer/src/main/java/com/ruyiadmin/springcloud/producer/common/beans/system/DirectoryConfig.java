package com.ruyiadmin.springcloud.producer.common.beans.system;

import cn.hutool.core.io.FileUtil;
import com.ruyiadmin.springcloud.producer.common.factories.RuYiAdminYamlPropertySourceFactory;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

/**
 * <p>
 * 系统目录配置类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-02
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "directoryconfig")
@PropertySource(value = {"classpath:appSettings.yaml"}, factory = RuYiAdminYamlPropertySourceFactory.class)
@Data
public class DirectoryConfig {
    //系统模板目录
    private String TemplateDirectory;
    //存储类型：相对路径或者绝对路径（Relative/Absolute）
    private String StorageType;
    //上传文件存储目录
    private String StoragePath;
    //上传文件存储临时目录，可清空
    private String TempPath;
    //审计日志目录
    private String MonitoringLogsPath;
    //业务附件目录
    private String BusinessAttachmentPath;

    //region 获取模板路径

    /**
     * 获取模板路径
     *
     * @return 模板路径
     */
    public String getTemplateDirectory() {
        try {
            return ResourceUtils.getURL("classpath:").getPath() + "\\" + this.TemplateDirectory;
        } catch (Exception e) {
            return StringUtils.EMPTY;
        }
    }

    //endregion

    //region 获取存储路径

    /**
     * 获取存储路径
     *
     * @return 存储路径
     */
    public String getStoragePath() {
        if (this.StorageType.equals("Relative")) {
            String path = System.getProperty("user.dir") + this.StoragePath;
            FileUtil.mkdir(path);
            return path;
        } else if (this.StorageType.equals("Absolute")) {
            FileUtil.mkdir(this.StoragePath);
            return this.StoragePath;
        }
        return StringUtils.EMPTY;
    }

    //endregion

    //region 获取临时目录

    /**
     * 获取临时目录
     *
     * @return 临时目录
     */
    public String getTempPath() {
        String path = this.getStoragePath() + "\\" + this.TempPath;
        FileUtil.mkdir(path);
        return path;
    }

    //endregion

    //region 清空临时目录

    /**
     * 清空临时目录
     */
    public void cleanTempPath() {
        FileUtil.clean(this.getTempPath());
    }

    //endregion

    //region 获取审计日志所在目录

    /**
     * 获取审计日志所在目录
     *
     * @return 审计日志所在目录
     */
    public String getMonitoringLogsPath() {
        String path = this.getStoragePath() + "\\" + this.MonitoringLogsPath;
        FileUtil.mkdir(path);
        return path;
    }

    //endregion

    //region 获取业务附件目录

    /**
     * 获取业务附件目录
     *
     * @return 业务附件目录
     */
    public String getBusinessAttachmentPath() {
        String path = this.getStoragePath() + "\\" + this.BusinessAttachmentPath;
        FileUtil.mkdir(path);
        return path;
    }

    //endregion
}
