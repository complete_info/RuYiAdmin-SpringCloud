package com.ruyiadmin.springcloud.producer.domain.entity.system;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.ruyiadmin.springcloud.producer.domain.entity.base.RuYiAdminBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 系统附件模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
@TableName("sys_attachment")
@ApiModel(value = "SysAttachment对象", description = "系统附件表")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysAttachment extends RuYiAdminBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId("ID")
    @NotNull
    private String id;

    @ApiModelProperty("附件名称")
    @TableField("FILE_NAME")
    @NotNull
    @Max(1024)
    private String fileName;

    @ApiModelProperty("文件大小")
    @TableField("FILE_SIZE")
    @NotNull
    private Double fileSize;

    @ApiModelProperty("存储路径")
    @TableField("FILE_PATH")
    @NotNull
    @Max(512)
    private String filePath;

    @ApiModelProperty("业务编号")
    @TableField("BUSINESS_ID")
    @NotNull
    private String businessId;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @NotNull
    @TableLogic
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

}
