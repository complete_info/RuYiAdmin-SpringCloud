package com.ruyiadmin.springcloud.producer.domain.entity.system;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.ruyiadmin.springcloud.producer.domain.entity.base.RuYiAdminBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 数据字典模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Getter
@Setter
@TableName("sys_code_table")
@ApiModel(value = "SysCodeTable对象", description = "数据字典表")
public class SysCodeTable extends RuYiAdminBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId("ID")
    @NotNull
    private String id;

    @ApiModelProperty("名称")
    @TableField("CODE_NAME")
    @NotNull
    @Max(128)
    private String codeName;

    @ApiModelProperty("编码")
    @TableField("CODE")
    @NotNull
    @Max(128)
    private String code;

    @ApiModelProperty("值")
    @TableField("VALUE")
    @Max(256)
    private String value;

    @ApiModelProperty("父键")
    @TableField("PARENT_ID")
    private String parentId;

    @ApiModelProperty("序号")
    @TableField("SERIAL_NUMBER")
    private Integer serialNumber;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @TableLogic
    @NotNull
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

    @ApiModelProperty("预留字段1")
    @TableField("EXTEND1")
    @Max(36)
    private String extend1;

    @ApiModelProperty("预留字段2")
    @TableField("EXTEND2")
    @Max(64)
    private String extend2;

    @ApiModelProperty("预留字段3")
    @TableField("EXTEND3")
    @Max(128)
    private String extend3;

    @ApiModelProperty("预留字段4")
    @TableField("EXTEND4")
    @Max(256)
    private String extend4;

    @ApiModelProperty("预留字段5")
    @TableField("EXTEND5")
    @Max(512)
    private String extend5;

}
