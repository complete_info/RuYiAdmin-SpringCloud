package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 审计日志表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysLogService extends IService<SysLog> {

}
