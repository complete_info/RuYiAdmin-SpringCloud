package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruyiadmin.springcloud.producer.common.beans.system.SystemCacheConfig;
import com.ruyiadmin.springcloud.producer.common.components.core.RuYiRedisComponent;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryCondition;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysRoleDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysRole;
import com.ruyiadmin.springcloud.producer.repository.system.ISysRoleRepository;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SysRoleServiceImpl extends ServiceImpl<ISysRoleRepository, SysRole> implements ISysRoleService {

    //region 实现类私有属性

    private final RuYiRedisComponent redisUtils;
    private final SystemCacheConfig systemCacheConfig;
    private final ISysRoleRepository roleRepository;

    //endregion

    //region 加载系统角色缓存

    /**
     * 加载系统角色缓存
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void loadSysRoleCache() {
        List<SysRoleDTO> roles = this.roleRepository.queryAllSysRole();
        this.redisUtils.set(systemCacheConfig.getRoleCacheName(), JSON.toJSONString(roles));
        log.info("RuYiAdmin sys roles cache loaded");
    }

    //endregion

    //region 清理系统角色缓存

    /**
     * 清理系统角色缓存
     */
    @Override
    public void clearSysRoleCache() {
        this.redisUtils.del(systemCacheConfig.getRoleCacheName());
        log.info("RuYiAdmin sys roles cache cleared");
    }

    //endregion

    //region 查询机构角色

    /**
     * 查询机构角色
     *
     * @param queryCondition 查询条件
     * @return QueryResult
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public QueryResult<SysRoleDTO> queryOrgRoleInfo(QueryCondition queryCondition) {
        queryCondition.setSort(StringUtils.EMPTY);

        QueryWrapper<SysRoleDTO> wrapper = new QueryWrapper<>();
        queryCondition.getQueryWrapper(wrapper);
        Page<SysRoleDTO> page = new Page<>(queryCondition.getPageIndex(), queryCondition.getPageSize());

        IPage<SysRoleDTO> records = this.roleRepository.queryOrgRoleInfo(page, wrapper);

        return QueryResult.success(records.getTotal(), records.getRecords());
    }

    //endregion

}
