package com.ruyiadmin.springcloud.producer.domain.dto.system;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 导入明细DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-08-13
 */
@Data
public class ImportConfigDetailDTO {
    @ApiModelProperty("数据类型，0：小数，1：整数，2：文本，3：日期，4：时间")
    @TableField("DATA_TYPE")
    private Integer dataType;

    @ApiModelProperty("所在列")
    @TableField("CELLS")
    private String cells;

    @ApiModelProperty("是否必填项，0：否，1：是")
    @TableField("REQUIRED")
    private Integer required;

    @ApiModelProperty("最大值")
    @TableField("MAX_VALUE")
    private Double maxValue;

    @ApiModelProperty("最小值")
    @TableField("MIN_VALUE")
    private Double minValue;

    @ApiModelProperty("小数位上限")
    @TableField("DECIMAL_LIMIT")
    private Integer decimalLimit;

    @ApiModelProperty("枚举列表")
    @TableField("TEXT_ENUM")
    private String textEnum;

    @ApiModelProperty("扩展字段")
    @TableField("EXTEND1")
    private String extend1;

    @ApiModelProperty("扩展字段")
    @TableField("EXTEND2")
    private String extend2;

    @ApiModelProperty("扩展字段")
    @TableField("EXTEND3")
    private String extend3;

    @ApiModelProperty("序号")
    @TableField("SERIAL_NUMBER")
    private Integer serialNumber;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    private String remark;
}
