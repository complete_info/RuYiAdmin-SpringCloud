package com.ruyiadmin.springcloud.producer.common.configurations.core;

import com.ruyiadmin.springcloud.producer.common.beans.system.ActiveMQConfig;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.jms.Queue;
import javax.jms.Topic;

/**
 * <p>
 * RuYiAdmin Bean配置
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-29
 */
@Configuration
public class RuYiAdminBeanConfiguration {

    @Resource
    private ActiveMQConfig activeMQConfig;

    //region 定义存放mq消息的队列、主题

    @Bean
    public Queue queue() {
        return new ActiveMQQueue(activeMQConfig.getQueueName());
    }

    @Bean
    public Topic topic() {
        return new ActiveMQTopic(activeMQConfig.getTopicName());
    }

    //endregion

}
