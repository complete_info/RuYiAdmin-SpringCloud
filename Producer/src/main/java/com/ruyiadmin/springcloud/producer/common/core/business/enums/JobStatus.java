package com.ruyiadmin.springcloud.producer.common.core.business.enums;


/**
 * <p>
 * 任务状态枚举
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-02
 */
public enum JobStatus {
    //已启用
    Started,

    //运行中
    Running,

    //执行中
    Executing,

    //执行完成
    Completed,

    //任务计划中
    Planning,

    //已停止
    Stopped
}

