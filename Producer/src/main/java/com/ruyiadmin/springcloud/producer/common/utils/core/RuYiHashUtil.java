package com.ruyiadmin.springcloud.producer.common.utils.core;

import lombok.experimental.UtilityClass;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Hash函数工具类
 *
 * @author RuYiAdmin
 * @since 2022-09-01
 */
@UtilityClass
public class RuYiHashUtil {

    /**
     * SHA256加密
     *
     * @param strText 输入
     * @return 输出
     */
    public String SHA256(final String strText) {
        return SHA(strText, "SHA-256");
    }

    /**
     * SHA512加密
     *
     * @param strText 输入
     * @return 输出
     */
    public String SHA512(final String strText) {
        return SHA(strText, "SHA-512");
    }

    /**
     * MD5加密
     *
     * @param strText 输入
     * @return 输出
     */
    public String SHAMD5(String strText) {
        return SHA(strText, "MD5");
    }

    /**
     * SHA加密
     *
     * @param strText 输入
     * @param strType 加密类型
     * @return 输出
     */
    private String SHA(final String strText, final String strType) {
        // 返回值
        String strResult = null;

        // 是否是有效字符串
        if (strText != null && strText.length() > 0) {
            try {
                // SHA 加密开始
                // 创建加密对象
                MessageDigest messageDigest = MessageDigest.getInstance(strType);
                // 传入要加密的字符串
                messageDigest.update(strText.getBytes());
                // 得到byte数组
                byte byteBuffer[] = messageDigest.digest();

                // 將 byte 为string
                StringBuffer strHexString = new StringBuffer();
                for (int i = 0; i < byteBuffer.length; i++) {
                    String hex = Integer.toHexString(0xff & byteBuffer[i]);
                    if (hex.length() == 1) {
                        strHexString.append('0');
                    }
                    strHexString.append(hex);
                }
                // 得到返回結果
                strResult = strHexString.toString();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        return strResult;
    }
}
