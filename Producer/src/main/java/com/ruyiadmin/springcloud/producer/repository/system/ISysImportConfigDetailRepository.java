package com.ruyiadmin.springcloud.producer.repository.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysImportConfigDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 导入配置子表 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Mapper
public interface ISysImportConfigDetailRepository extends BaseMapper<SysImportConfigDetail> {

}
