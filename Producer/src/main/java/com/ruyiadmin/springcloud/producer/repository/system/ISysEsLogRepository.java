package com.ruyiadmin.springcloud.producer.repository.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysEsLog;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * <p>
 * 审计日志表ES Repository 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysEsLogRepository extends ElasticsearchRepository<SysEsLog, String> {
}
