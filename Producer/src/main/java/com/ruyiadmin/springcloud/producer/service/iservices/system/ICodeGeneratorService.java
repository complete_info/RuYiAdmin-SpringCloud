package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.ActionResult;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.CodeGeneratorDTO;
import com.ruyiadmin.springcloud.producer.domain.dto.system.DbSchemaInfoDTO;

import java.io.IOException;

/**
 * <p>
 * 代码生成器 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-05
 */
public interface ICodeGeneratorService extends IService<DbSchemaInfoDTO> {
    /**
     * 查询数据库空间信息
     */
    QueryResult<DbSchemaInfoDTO> querySchemaInfo();

    /**
     * 生成项目代码
     *
     * @param codeGenerator 模型DTO
     * @return ActionResult
     */
    ActionResult codeGenerate(CodeGeneratorDTO codeGenerator) throws IOException;
}
