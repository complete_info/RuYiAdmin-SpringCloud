package com.ruyiadmin.springcloud.producer.repository.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysAttachment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统附件表 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
@Mapper
public interface ISysAttachmentRepository extends BaseMapper<SysAttachment> {

}
