package com.ruyiadmin.springcloud.producer.common.core.system.enums;

/**
 * <p>
 * 系统消息类型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-29
 */
public enum MessageType {
    //强制下线
    ForceLogout,

    //通知
    Notification,

    //公告
    Announcement,

    //广播
    Broadcast
}
