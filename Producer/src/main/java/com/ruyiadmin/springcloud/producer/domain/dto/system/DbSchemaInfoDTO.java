package com.ruyiadmin.springcloud.producer.domain.dto.system;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;

/**
 * Db Schema DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-08-05
 */
@Data
public class DbSchemaInfoDTO {
    @ApiModelProperty("数据库名称")
    @TableField("TABLE_SCHEMA")
    private String tableSchema;

    @ApiModelProperty("表名称")
    @TableField("TABLE_NAME")
    private String tableName;

    @ApiModelProperty("表注释")
    @TableField("TABLE_COMMENT")
    private String tableComment;

    @ApiModelProperty("创建时间")
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;

    @ApiModelProperty("字段名称")
    @TableField("COLUMN_NAME")
    private String columnName;

    @ApiModelProperty("数据类型")
    @TableField("DATA_TYPE")
    private String dataType;

    @ApiModelProperty("字段注释")
    @TableField("COLUMN_COMMENT")
    private String columnComment;

    @ApiModelProperty("是否为空")
    @TableField("IS_NULLABLE")
    private String isNullable;

    @ApiModelProperty("最大长度")
    @TableField("CHARACTER_MAXIMUM_LENGTH")
    private String characterMaximumLength;

    //region 获取数据类型

    /**
     * 获取数据类型
     *
     * @return 转译类型
     */
    public String getColumnDataType() {
        if (StringUtils.isEmpty(this.getDataType())) {
            return StringUtils.EMPTY;
        }

        if (this.getDataType().contains("(")) {
            if (this.getDataType().contains("number")) {
                this.setDataType("number(");
            } else {
                this.setDataType(this.getDataType().split("\\(")[0]);
            }
        }

        if (this.getDataType().equals("varchar") && this.getDataType().equals("36")) {
            return "Guid";
        }

        switch (this.getDataType()) {
            case "tinyint":
            case "smallint":
            case "mediumint":
            case "int":
            case "integer":
            case "number":
            case "int4":
                return "int";

            case "double":
            case "number(":
                return "Double";

            case "float":
            case "float8":
                return "float";

            case "decimal":
            case "numeric":
            case "real":
                return "decimal";

            case "bit":
                return "bool";

            case "date":
            case "time":
            case "year":
            case "datetime":
            case "timestamp":
            case "datetime2":
                return "DateTime";

            case "tinyblob":
            case "blob":
            case "mediumblob":
            case "longblob":
            case "binary":
            case "varbinary":
            case "bytea":
                return "byte[]";

            case "char":
            case "varchar":
            case "nvarchar2":
            case "tinytext":
            case "text":
            case "mediumtext":
            case "longtext":
            case "clob":
            case "nvarchar":
                return "String";

            case "uuid":
            case "uniqueidentifier":
                return "Guid";

            case "point":
            case "linestring":
            case "polygon":
            case "geometry":
            case "multipoint":
            case "multilinestring":
            case "multipolygon":
            case "geometrycollection":
            case "enum":
            case "set":

            default:
                return StringUtils.EMPTY;
        }
    }

    //endregion
}
