package com.ruyiadmin.springcloud.producer.common.core.business.enums;

/**
 * <p>
 * 操作类型枚举
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-31
 */
public enum OperationType {
    //查询列表
    QueryList,

    //查询实体
    QueryEntity,

    //新增实体
    AddEntity,

    //编辑实体
    EditEntity,

    //逻辑删除实体
    DeleteEntity,

    //物理删除实体
    RemoveEntity,

    //上传文件
    UploadFile,

    //下载文件
    DownloadFile,

    //导入数据
    ImportData,

    //导出数据
    ExportData,

    //菜单授权
    MenuAuthorization,

    //用户授权
    PermissionAuthorization,

    //打印
    Print,

    //登录
    Logon,

    //登出
    Logout,

    //强制登出
    ForceLogout,

    //更新密码
    UpdatePassword,

    //启动计划任务
    StartScheduleJob,

    //暂停计划任务
    PauseScheduleJob,

    //恢复计划任务
    ResumeScheduleJob,

    //权限下放
    DelegatePermission,

    //生成代码
    GenerateCode,

    //Sql注入攻击
    SqlInjectionAttack,

    //Token劫持
    TokenHijacked,

    //统一认证
    UnifiedAuthentication,

    //统一认证授权
    UnifiedAuthorization,

    //解除统一授权
    RemoveUnifiedAuthorization,

    //获取同步口令
    GetSyncToken,

    //同步新增用户
    SyncAddUser,

    //同步编辑用户
    SyncEditUser,

    //同步删除用户
    SyncDeleteUser
}
