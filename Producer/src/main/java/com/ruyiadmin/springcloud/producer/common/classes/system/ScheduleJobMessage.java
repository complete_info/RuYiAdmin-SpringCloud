package com.ruyiadmin.springcloud.producer.common.classes.system;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 系统分布式任务消息类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleJobMessage {
    //计划任务编号
    private String JobId;
    //节点编号
    private int GroupId;
    //执行动作
    private String Action;
    //操作人
    private String UserId;
}
