package com.ruyiadmin.springcloud.producer.common.components.system;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.HttpUtil;
import com.ruyiadmin.springcloud.producer.common.constants.system.Guid;
import com.ruyiadmin.springcloud.producer.common.components.core.RuYiSessionContext;
import com.ruyiadmin.springcloud.producer.common.core.business.enums.DeletionType;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysUserDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysLog;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.UUID;

/**
 * <p>
 * 系统日志工具类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-20
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class RuYiLogComponent {

    //region 工具类私有属性

    private final RuYiSessionContext sessionContext;

    //endregion

    //region 获取日志信息

    /**
     * 获取日志信息
     *
     * @return 日志信息
     * @throws Exception 异常信息
     */
    public SysLog getSysLog() throws Exception {

        //获取请求url,ip,httpMethod
        HttpServletRequest request = ((ServletRequestAttributes) Objects
                .requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();

        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("user-agent"));
        String clientType = userAgent.getOperatingSystem().getDeviceType().toString();//客户端类型  手机、电脑、平板
        String os = userAgent.getOperatingSystem().getName();//操作系统类型
        String browser = userAgent.getBrowser().toString();//浏览器类型

        SysUserDTO user = sessionContext.getCurrentUserInfo();

        SysLog sysLog = new SysLog();

        sysLog.setId(UUID.randomUUID().toString());
        sysLog.setUserId(user.getId());
        sysLog.setUserName(user.getDisplayName() + "/" + user.getLogonName());
        sysLog.setOrgId(StringUtils.isEmpty(user.getOrgId()) ? Guid.Empty : user.getOrgId());
        sysLog.setOrgName(StringUtils.isEmpty(user.getOrgName()) ? StringUtils.EMPTY : user.getOrgName());
        sysLog.setSystem(clientType + "，" + os);
        sysLog.setBrowser(browser);
        sysLog.setIp(ServletUtil.getClientIP(request));
        sysLog.setOperationType(0);
        sysLog.setRequestMethod(request.getMethod());
        sysLog.setRequestUrl(URLUtil.getPath(request.getRequestURI()));
        sysLog.setParams(HttpUtil.toParams(request.getParameterMap()));
        sysLog.setResult(StringUtils.EMPTY);
        sysLog.setOldValue(StringUtils.EMPTY);
        sysLog.setNewValue(StringUtils.EMPTY);
        sysLog.setRemark(sysLog.getUserName() + "于" + LocalDateTimeUtil.now()
                + "访问了" + sysLog.getRequestUrl() + "接口");
        sysLog.setIsdel(DeletionType.Undeleted.ordinal());
        sysLog.setCreator(user.getId());
        sysLog.setCreateTime(LocalDateTimeUtil.now());
        sysLog.setModifier(user.getId());
        sysLog.setModifyTime(LocalDateTimeUtil.now());
        sysLog.setVersionId(UUID.randomUUID().toString());

        return sysLog;
    }

    //endregion

}
