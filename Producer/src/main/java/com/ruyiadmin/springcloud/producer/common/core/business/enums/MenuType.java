package com.ruyiadmin.springcloud.producer.common.core.business.enums;

/**
 * <p>
 * 菜单类型枚举
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-31
 */
public enum MenuType {
    //菜单
    Menu,
    //按钮
    Button,
    //视图
    View
}
