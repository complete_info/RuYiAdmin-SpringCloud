package com.ruyiadmin.springcloud.producer.service.iservices.business.module;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.ActionResult;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryCondition;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.business.module.BizUserModuleDTO;
import com.ruyiadmin.springcloud.producer.domain.dto.system.LoginDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.business.module.BizUserModule;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 模块与用户关系表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface IBizUserModuleService extends IService<BizUserModule> {

    /**
     * <p>
     * 查询业务用户与模块信息
     * </p>
     *
     * @param queryCondition 查询条件
     * @return 查询结果
     */
    QueryResult<BizUserModuleDTO> queryUserModule(QueryCondition queryCondition);

    /**
     * 统一认证用户登录
     *
     * @param loginDTO 登录信息
     * @return 执行结果
     */
    ActionResult logon(LoginDTO loginDTO) throws Exception;

    /**
     * 匿名获取同步口令
     *
     * @param login 登录信息
     * @return ActionResult
     */
    ActionResult getToken(LoginDTO login) throws Exception;
}
