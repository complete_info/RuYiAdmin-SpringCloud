package com.ruyiadmin.springcloud.producer.domain.dto.system;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import java.time.LocalDateTime;

/**
 * 角色DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-08-05
 */
@Data
public class SysRoleDTO {

    @ApiModelProperty("主键")
    @TableId("ID")
    private String id;

    @ApiModelProperty("角色名称")
    @TableField("ROLE_NAME")
    private String roleName;

    @ApiModelProperty("序号")
    @TableField("SERIAL_NUMBER")
    private Integer serialNumber;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    private String remark;

    @ApiModelProperty("标志位")
    @TableField("ISDEL")
    @TableLogic
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField("CREATOR")
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER")
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @Version    // 乐观锁注解
    private String versionId;

    @ApiModelProperty("预留字段1")
    @TableField("EXTEND1")
    @Max(36)
    private String extend1;

    @ApiModelProperty("预留字段2")
    @TableField("EXTEND2")
    @Max(64)
    private String extend2;

    @ApiModelProperty("预留字段3")
    @TableField("EXTEND3")
    @Max(128)
    private String extend3;

    @ApiModelProperty("预留字段4")
    @TableField("EXTEND4")
    @Max(256)
    private String extend4;

    @ApiModelProperty("预留字段5")
    @TableField("EXTEND5")
    @Max(512)
    private String extend5;

    @ApiModelProperty("机构编号")
    @TableField("ORG_ID")
    private String orgId;

    @ApiModelProperty("机构名称")
    @TableField(exist = false)
    private String orgName;
}
