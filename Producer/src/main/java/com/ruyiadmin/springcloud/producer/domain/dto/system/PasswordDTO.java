package com.ruyiadmin.springcloud.producer.domain.dto.system;

import lombok.Data;

/**
 * 密码DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-08-05
 */
@Data
public class PasswordDTO {
    private String userId;
    private String password;
    private String salt;
}
