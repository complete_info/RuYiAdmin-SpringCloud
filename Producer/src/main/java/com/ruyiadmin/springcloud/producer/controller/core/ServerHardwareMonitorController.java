package com.ruyiadmin.springcloud.producer.controller.core;

import cn.hutool.core.lang.Dict;
import com.alibaba.fastjson.JSON;
import com.ruyiadmin.springcloud.producer.common.annotations.system.Log;
import com.ruyiadmin.springcloud.producer.common.annotations.system.Permission;
import com.ruyiadmin.springcloud.producer.common.core.business.enums.OperationType;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.ActionResult;
import com.ruyiadmin.springcloud.producer.common.utils.system.RuYiHardwareUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 服务器监控 前端控制器
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-22
 */
@RestController
@RequestMapping("/ServerHardwareMonitor")
@Api(tags = "系统服务器监控服务")
public class ServerHardwareMonitorController {

    //region 查询服务器信息

    @GetMapping("/Get")
    @ApiOperation(value = "查询服务器信息")
    @Log(OperationType = OperationType.QueryEntity)
    @Permission(permission = "query:hardware:info")
    public ActionResult queryHardwareInfo() {
        Dict hardwareInfo = RuYiHardwareUtil.getHardwareInfo();
        return ActionResult.success(JSON.toJSON(hardwareInfo));
    }

    //endregion

}
