package com.ruyiadmin.springcloud.producer.repository.system;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysNotificationDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysAnnouncement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 通知公告表 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
@Mapper
public interface ISysAnnouncementRepository extends BaseMapper<SysAnnouncement> {
    /**
     * 分页查询当前用户通知
     *
     * @param page         分页信息
     * @param queryWrapper 过滤条件
     * @param userId       当前用户编号
     * @return 分页数据集合
     */
    IPage<SysNotificationDTO> getNotifications(IPage<SysNotificationDTO> page,
                                               @Param(Constants.WRAPPER) Wrapper<SysNotificationDTO> queryWrapper,
                                               @Param("userId") String userId);
}
