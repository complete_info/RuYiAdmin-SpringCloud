package com.ruyiadmin.springcloud.producer.domain.dto.system;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 机构用户树DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-08-05
 */
@Data
public class OrgUserTreeDTO {
    @ApiModelProperty("编号")
    @TableField("ID")
    private String id;

    @ApiModelProperty("名称")
    @TableField("Name")
    private String name;

    @ApiModelProperty("类型，1，机构，2，用户")
    @TableField("Type")
    private int type;

    @ApiModelProperty("序号")
    @TableField("SERIAL_NUMBER")
    private int serialNumber;

    @ApiModelProperty("子集")
    @TableField(exist = false)
    private List<OrgUserTreeDTO> children;
}
