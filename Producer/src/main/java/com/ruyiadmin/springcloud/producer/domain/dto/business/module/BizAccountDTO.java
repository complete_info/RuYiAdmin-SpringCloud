package com.ruyiadmin.springcloud.producer.domain.dto.business.module;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * <p>
 * 同步账号DTO模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-09-06
 */
@Data
public class BizAccountDTO {
    @ApiModelProperty("用户编号")
    @TableId("ID")
    @NotNull
    private String id;

    @ApiModelProperty("模块编号")
    @TableField("MODULE_ID")
    @NotNull
    private String moduleId;

    @ApiModelProperty("用户登录账号")
    @TableField("USER_LOGON_NAME")
    @NotNull
    @Max(128)
    private String userLogonName;

    @ApiModelProperty("用户名称")
    @TableField("USER_DISPLAY_NAME")
    @NotNull
    @Max(128)
    private String userDisplayName;

    @ApiModelProperty("用户密码")
    @TableField("USER_PASSWORD")
    @NotNull
    @Max(512)
    private String userPassword;

    @ApiModelProperty("座机")
    @TableField("TELEPHONE")
    @Max(45)
    private String telephone;

    @ApiModelProperty("手机")
    @TableField("MOBILEPHONE")
    @Max(45)
    private String mobilephone;

    @ApiModelProperty("邮箱")
    @TableField("EMAIL")
    @Max(45)
    private String email;

    @ApiModelProperty("是否启用，0：禁用，1：启用")
    @TableField("IS_ENABLED")
    private Integer isEnabled;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @TableLogic
    @NotNull
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

    @ApiModelProperty("口令")
    @NotNull
    private String token;

}
