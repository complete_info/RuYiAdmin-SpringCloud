package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysOrgUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 机构用户关系表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysOrgUserService extends IService<SysOrgUser> {

}
