package com.ruyiadmin.springcloud.producer.domain.entity.business.module;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.ruyiadmin.springcloud.producer.domain.entity.base.RuYiAdminBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 模块模型
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Getter
@Setter
@TableName("biz_module")
@ApiModel(value = "BizModule对象", description = "模块表")
@NoArgsConstructor
@AllArgsConstructor
public class BizModule extends RuYiAdminBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("模块编号")
    @TableId("ID")
    @NotNull
    private String id;

    @ApiModelProperty("模块名称")
    @TableField("MODULE_NAME")
    @NotNull
    @Max(512)
    private String moduleName;

    @ApiModelProperty("模块简称")
    @TableField("MODULE_SHORT_NAME")
    @NotNull
    @Max(256)
    private String moduleShortName;

    @ApiModelProperty("模块英文简称")
    @TableField("MODULE_SHORT_NAME_EN")
    @NotNull
    @Max(128)
    private String moduleShortNameEn;

    @ApiModelProperty("采用HTTP协议，HTTP或者HTTPS")
    @TableField("MODULE_PROTOCOL")
    @NotNull
    @Max(15)
    private String moduleProtocol;

    @ApiModelProperty("模块地址：ip或者域名")
    @TableField("MODULE_ADDRESS")
    @NotNull
    @Max(256)
    private String moduleAddress;

    @ApiModelProperty("模块端口")
    @TableField("MODULE_PORT")
    private Integer modulePort;

    @ApiModelProperty("模块logo图片位置")
    @TableField("MODULE_LOGO_ADDRESS")
    private String moduleLogoAddress;

    @ApiModelProperty("模块单点登录地址")
    @TableField("MODULE_SSO_ADDRESS")
    @Max(512)
    private String moduleSsoAddress;

    @ApiModelProperty("模块待办地址")
    @TableField("MODULE_TODO_ADDRESS")
    @Max(512)
    private String moduleTodoAddress;

    @ApiModelProperty("序号")
    @TableField("SERIAL_NUMBER")
    private Integer serialNumber;

    @ApiModelProperty("备注")
    @TableField("REMARK")
    @Max(512)
    private String remark;

    @ApiModelProperty("标志位")
    @TableField(value = "ISDEL", fill = FieldFill.INSERT)
    @TableLogic
    @NotNull
    private Integer isdel;

    @ApiModelProperty("创建人")
    @TableField(value = "CREATOR", fill = FieldFill.INSERT)
    @NotNull
    private String creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @NotNull
    private LocalDateTime createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "MODIFIER", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private String modifier;

    @ApiModelProperty("修改时间")
    @TableField(value = "MODIFY_TIME", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    private LocalDateTime modifyTime;

    @ApiModelProperty("版本号")
    @TableField(value = "VERSION_ID", fill = FieldFill.INSERT_UPDATE)
    @NotNull
    @Version    // 乐观锁注解
    private String versionId;

}
