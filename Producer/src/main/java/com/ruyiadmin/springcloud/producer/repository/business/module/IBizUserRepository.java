package com.ruyiadmin.springcloud.producer.repository.business.module;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruyiadmin.springcloud.producer.domain.dto.business.module.BizUserModuleDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.business.module.BizUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 模块用户表 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Mapper
public interface IBizUserRepository extends BaseMapper<BizUser> {

    /**
     * 分页查询离态用户列表
     *
     * @param page    分页信息
     * @param wrapper 查询条件
     * @return 分页集合
     */
    IPage<BizUserModuleDTO> queryUserNonModule(Page<BizUserModuleDTO> page,
                                               @Param(Constants.WRAPPER) QueryWrapper<BizUserModuleDTO> wrapper);

    /**
     * <p>
     * 查询所有业务用户
     * </p>
     *
     * @return 业务用户集合
     */
    List<BizUser> queryAllBizUsers();
}
