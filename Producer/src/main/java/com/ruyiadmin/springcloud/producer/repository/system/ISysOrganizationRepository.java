package com.ruyiadmin.springcloud.producer.repository.system;

import com.ruyiadmin.springcloud.producer.domain.dto.system.OrgUserTreeDTO;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysOrganizationDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysOrganization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 机构表 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@Mapper
public interface ISysOrganizationRepository extends BaseMapper<SysOrganization> {

    /**
     * 查询所有机构信息
     *
     * @return 机构信息集合
     */
    List<SysOrganizationDTO> queryAllSysOrg();

    /**
     * 查询机构树
     *
     * @param orgId 机构编号
     * @return 机构树
     */
    List<OrgUserTreeDTO> queryOrgTree(@Param("orgId") String orgId);

    /**
     * 查询用户树
     *
     * @param orgId 机构编号
     * @return 用户树
     */
    List<OrgUserTreeDTO> queryUserTree(@Param("orgId") String orgId);

    /**
     * 查询机构树子节点
     *
     * @param orgId 机构编号
     * @return 机构用户树
     */
    List<OrgUserTreeDTO> queryOrgByParent(@Param("orgId") String orgId);
}
