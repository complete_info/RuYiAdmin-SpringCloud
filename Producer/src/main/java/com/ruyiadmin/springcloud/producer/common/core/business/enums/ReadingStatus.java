package com.ruyiadmin.springcloud.producer.common.core.business.enums;

/**
 * <p>
 * 阅读状态枚举
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-31
 */
public enum ReadingStatus {
    //未读
    Unread,
    //已读
    Read
}
