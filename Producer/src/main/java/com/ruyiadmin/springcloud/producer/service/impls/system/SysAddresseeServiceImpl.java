package com.ruyiadmin.springcloud.producer.service.impls.system;

import com.ruyiadmin.springcloud.producer.common.components.core.RuYiSessionContext;
import com.ruyiadmin.springcloud.producer.common.core.business.enums.DeletionType;
import com.ruyiadmin.springcloud.producer.common.core.business.enums.ReadingStatus;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.ActionResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysUserDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysAddressee;
import com.ruyiadmin.springcloud.producer.repository.system.ISysAddresseeRepository;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysAddresseeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 收件人表 服务实现类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
@Service
@RequiredArgsConstructor
public class SysAddresseeServiceImpl extends ServiceImpl<ISysAddresseeRepository, SysAddressee>
        implements ISysAddresseeService {

    //region 实现类私有属性

    private final ISysAddresseeRepository addresseeRepository;
    private final RuYiSessionContext sessionContext;

    //endregion

    //region 更改通知收件人阅读状态

    /**
     * 更改通知收件人阅读状态
     *
     * @param notificationId 通知编号
     * @return ActionResult
     * @throws Exception 异常
     */
    @Override
    public ActionResult updateNotificationStatus(String notificationId) throws Exception {
        SysUserDTO user = sessionContext.getCurrentUserInfo();
        List<SysAddressee> addressees = this.list().stream().
                filter(t -> t.getIsdel() == DeletionType.Undeleted.ordinal()).
                filter(t -> t.getBusinessId().equals(notificationId)).
                filter(t -> t.getUserId().equals(user.getId())).
                collect(Collectors.toList());
        if (addressees.size() > 0) {
            SysAddressee addressee = addressees.get(0);
            if (addressee.getStatus() == (ReadingStatus.Unread.ordinal())) {
                addressee.setStatus(ReadingStatus.Read.ordinal());
                this.addresseeRepository.updateById(addressee);
            }
        }
        return ActionResult.ok();
    }

    //endregion

}
