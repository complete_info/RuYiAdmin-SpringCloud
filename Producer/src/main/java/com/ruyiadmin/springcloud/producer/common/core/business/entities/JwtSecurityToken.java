package com.ruyiadmin.springcloud.producer.common.core.business.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtSecurityToken {
    private String accessToken;
    private String refreshToken;
}
