package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.OrgUserTreeDTO;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysOrganizationDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysOrganization;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 机构表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysOrganizationService extends IService<SysOrganization> {
    /**
     * 加载组织机构缓存
     */
    void loadSysOrgCache();

    /**
     * 清理组织机构缓存
     */
    void clearSysOrgCache();

    /**
     * 查询机构列表
     *
     * @return QueryResult
     * @throws Exception 异常
     */
    QueryResult<SysOrganizationDTO> getOrgTreeNodes() throws Exception;

    /**
     * 获取机构、用户树
     *
     * @return QueryResult
     * @throws Exception 异常
     */
    QueryResult<OrgUserTreeDTO> getOrgUserTree() throws Exception;

}
