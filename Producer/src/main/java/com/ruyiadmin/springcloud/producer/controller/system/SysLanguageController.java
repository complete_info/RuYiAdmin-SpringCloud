package com.ruyiadmin.springcloud.producer.controller.system;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.fastjson.JSON;
import com.ruyiadmin.springcloud.producer.common.annotations.core.AllowAnonymous;
import com.ruyiadmin.springcloud.producer.common.beans.system.SystemCacheConfig;
import com.ruyiadmin.springcloud.producer.common.components.core.RuYiRedisComponent;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryCondition;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.common.utils.system.RuYiSentinelUtil;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysLanguage;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysLanguageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * <p>
 * 系统语言 前端控制器
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@RestController
@RequestMapping("/LanguageManagement")
@Api(tags = "系统多语管理服务")
@RequiredArgsConstructor
public class SysLanguageController {

    //region 服务私有属性

    private final RuYiRedisComponent redisUtils;
    private final SystemCacheConfig systemCacheConfig;
    private final ISysLanguageService languageService;

    //endregion

    //region 分页查询多语信息

    @PostMapping("/Post")
    @ApiOperation(value = "分页查询多语信息")
    @AllowAnonymous
    //@Log(operationType = OperationType.QueryList)
    //@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public QueryResult<SysLanguage> queryByPage(@RequestBody QueryCondition queryCondition)
            throws ExecutionException, InterruptedException, BlockException {

        //region 从数据库查询多语

        //QueryWrapper<SysLanguage> wrapper = new QueryWrapper<>();//设置条件
        //queryCondition.getQueryWrapper(wrapper);//转化查询条件、转化排序条件
        //Page<SysLanguage> page = new Page<>(queryCondition.getPageIndex(), queryCondition.getPageSize());//初始化page

        //this.languageService.page(page, wrapper);//执行查询
        //long total = page.getTotal();//总数
        //List<SysLanguage> rs = page.getRecords();//结果

        //return QueryResult.success(total, rs);

        //endregion

        RuYiSentinelUtil.tryAcquire("QuerySysLanguages");

        CompletableFuture<QueryResult<SysLanguage>> future = CompletableFuture.supplyAsync(() -> {
            Object value = this.redisUtils.get(systemCacheConfig.getLanguageCacheName());
            List<SysLanguage> languages = JSON.parseArray(value.toString(), SysLanguage.class);
            return QueryResult.success(languages.size(), languages);
        });

        return future.get();
    }

    //endregion

}
