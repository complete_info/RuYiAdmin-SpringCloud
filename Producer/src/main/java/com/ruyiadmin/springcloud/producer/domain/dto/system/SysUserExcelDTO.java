package com.ruyiadmin.springcloud.producer.domain.dto.system;

import lombok.Data;

/**
 * 系统用户导入DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-09-14
 */
@Data
public class SysUserExcelDTO {
    private String displayName;
    private String logonName;
    private String sex;
    private String mobilephone;
    private String telephone;
    private String email;
}
