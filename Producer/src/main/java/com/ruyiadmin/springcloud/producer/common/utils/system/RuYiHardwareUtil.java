package com.ruyiadmin.springcloud.producer.common.utils.system;

import cn.hutool.core.lang.Dict;
import cn.hutool.system.SystemUtil;
import cn.hutool.system.oshi.OshiUtil;
import lombok.experimental.UtilityClass;

/**
 * <p>
 * 硬件信息工具类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-16
 */
@UtilityClass
public class RuYiHardwareUtil {

    //region 获取硬件信息

    /**
     * 获取硬件信息
     *
     * @return 字典
     */
    public Dict getHardwareInfo() {
        Dict dict = Dict.create();
        dict.set("CpuCount", OshiUtil.getHardware().getProcessor().getLogicalProcessorCount());
        dict.set("CpuLoad", OshiUtil.getCpuInfo().getUsed());
        dict.set("CpuTemperature", OshiUtil.getSensors().getCpuTemperature());
        dict.set("CpuInfo", OshiUtil.getCpuInfo());
        dict.set("LogicalDrives", OshiUtil.getHardware().getLogicalVolumeGroups());
        dict.set("DiskInfo", OshiUtil.getHardware().getDiskStores());
        dict.set("LocalUsedIP", SystemUtil.getHostInfo().getAddress());
        dict.set("RamInfo", OshiUtil.getHardware().getMemory());
        dict.set("OSArchitecture", SystemUtil.getOsInfo().getArch());
        dict.set("OSDescription", SystemUtil.getOsInfo().getName());
        dict.set("ProcessArchitecture", OshiUtil.getHardware().getProcessor().getProcessorIdentifier().getMicroarchitecture());
        dict.set("FrameworkDescription", SystemUtil.getJavaRuntimeInfo().getVersion());
        dict.set("Windows", SystemUtil.getOsInfo().isWindows());
        dict.set("Is64BitOperatingSystem", OshiUtil.getOs().getBitness() > 32);
        dict.set("Is64BitProcess", OshiUtil.getHardware().getProcessor().getProcessorIdentifier().isCpu64bit());
        dict.set("OSVersion", SystemUtil.getOsInfo().getVersion());
        dict.set("CpuCore", OshiUtil.getHardware().getProcessor().getPhysicalProcessorCount());
        dict.set("HostName", SystemUtil.getUserInfo().getName());
        return dict;
    }

    //endregion

}
