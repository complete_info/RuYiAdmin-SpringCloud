package com.ruyiadmin.springcloud.producer.common.core.business.enums;

/**
 * <p>
 * 是否类型枚举
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-31
 */
public enum YesNo {
    NO,
    YES
}
