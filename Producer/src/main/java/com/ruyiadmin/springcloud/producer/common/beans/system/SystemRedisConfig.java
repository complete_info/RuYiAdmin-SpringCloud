package com.ruyiadmin.springcloud.producer.common.beans.system;

import com.ruyiadmin.springcloud.producer.common.factories.RuYiAdminYamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 系统Redis配置类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-18
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "redisconfig")
@PropertySource(value = {"classpath:appSettings.yaml"}, factory = RuYiAdminYamlPropertySourceFactory.class)
@Data
public class SystemRedisConfig {
    //用户模糊搜索格式
    private String Pattern;
    //统一认证模糊搜索格式
    private String UnifiedAuthenticationPattern;
    //同步模糊搜索格式
    private String SynchronizationPattern;
    //是否支持哨兵
    private boolean SupportRedisSentinel;
    //哨兵集群
    private String RedisSentinels;
}
