package com.ruyiadmin.springcloud.producer.controller.business.module;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruyiadmin.springcloud.producer.common.annotations.system.Log;
import com.ruyiadmin.springcloud.producer.common.annotations.system.Permission;
import com.ruyiadmin.springcloud.producer.common.core.business.enums.OperationType;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.ActionResult;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryCondition;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.entity.business.module.BizAccount;
import com.ruyiadmin.springcloud.producer.service.iservices.business.module.IBizAccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * <p>
 * 模块API访问账号表 前端控制器
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
@RestController
@RequestMapping("/BizAccountManagement")
@Api(tags = "系统模块账号管理服务")
@RequiredArgsConstructor
public class BizAccountController {

    //region 管理服务私有属性

    private final IBizAccountService accountService;

    //endregion

    //region 查询模块同步账号列表

    @PostMapping("/Post")
    @ApiOperation(value = "查询模块同步账号列表")
    @Log(OperationType = OperationType.QueryList)
    @Permission(permission = "account:query:list")
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public QueryResult<BizAccount> queryByPage(@RequestBody QueryCondition queryCondition)
            throws ExecutionException, InterruptedException {
        CompletableFuture<QueryResult<BizAccount>> future = CompletableFuture.supplyAsync(() -> {
            QueryWrapper<BizAccount> wrapper = new QueryWrapper<>();//设置条件
            queryCondition.getQueryWrapper(wrapper);//转化查询条件、转化排序条件

            Page<BizAccount> page = new Page<>(queryCondition.getPageIndex(),
                    queryCondition.getPageSize());//初始化page

            this.accountService.page(page, wrapper);//执行查询
            long total = page.getTotal();//总数
            List<BizAccount> rs = page.getRecords();//结果

            return QueryResult.success(total, rs);
        });
        return future.get();
    }

    //endregion

    //region 查询同步账号信息

    @GetMapping("/GetById/{accountId}")
    @ApiOperation(value = "查询同步账号信息")
    @Log(OperationType = OperationType.QueryEntity)
    @Permission(permission = "account:query:list")
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ActionResult getById(@PathVariable("accountId") String accountId)
            throws ExecutionException, InterruptedException {
        CompletableFuture<ActionResult> future = CompletableFuture.supplyAsync(() ->
                ActionResult.success(this.accountService.getById(accountId)));
        return future.get();
    }

    //endregion

    //region 新增模块同步账号

    @PostMapping("/Add")
    @ApiOperation(value = "新增模块同步账号")
    @Log(OperationType = OperationType.AddEntity)
    @Permission(permission = "account:add:entity")
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ActionResult add(@Valid @RequestBody BizAccount bizAccount) {
        return ActionResult.success(this.accountService.save(bizAccount));
    }

    //endregion

    //region 编辑模块同步账号

    @PutMapping("/Put")
    @ApiOperation(value = "编辑模块同步账号")
    @Log(OperationType = OperationType.EditEntity)
    @Permission(permission = "account:edit:entity")
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ActionResult edit(@Valid @RequestBody BizAccount bizAccount) {
        return ActionResult.success(this.accountService.updateById(bizAccount));
    }

    //endregion

    //region 批量删除模块同步账号

    @DeleteMapping("/DeleteRange/{ids}")
    @ApiOperation(value = "批量删除模块同步账号")
    @Log(OperationType = OperationType.DeleteEntity)
    @Permission(permission = "account:del:entities")
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ActionResult deleteRange(@PathVariable("ids") String ids) {
        List<String> array = Arrays.asList(ids.split(","));
        return ActionResult.success(this.accountService.removeByIds(array));
    }

    //endregion

}
