package com.ruyiadmin.springcloud.producer.repository.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统记录表 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-09-04
 */
@Mapper
public interface ISysRecordRepository extends BaseMapper<SysRecord> {

}
