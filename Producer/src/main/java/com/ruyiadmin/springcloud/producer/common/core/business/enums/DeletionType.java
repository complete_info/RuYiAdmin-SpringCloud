package com.ruyiadmin.springcloud.producer.common.core.business.enums;

/**
 * <p>
 * 删除类型枚举
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-12
 */
public enum DeletionType {
    //未删除的
    Undeleted,

    //删除的
    Deleted
}
