package com.ruyiadmin.springcloud.producer.common.core.system.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.base.BaseDomain;
import com.ruyiadmin.springcloud.producer.common.core.system.enums.DataType;
import com.ruyiadmin.springcloud.producer.common.core.system.enums.QueryMethod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 系统查询项目类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-15
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class QueryItem extends BaseDomain {
    //字段
    @JsonProperty(value = "Field")
    private String field;
    //数据类型
    @JsonProperty(value = "DataType")
    private DataType dataType;
    //查询方法
    @JsonProperty(value = "QueryMethod")
    private QueryMethod queryMethod;
    //字段值
    @JsonProperty(value = "Value")
    private Object value;
}
