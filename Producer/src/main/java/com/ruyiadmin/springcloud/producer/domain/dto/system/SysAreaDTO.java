package com.ruyiadmin.springcloud.producer.domain.dto.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysArea;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 行政区域DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-07-17
 */
@Data
public class SysAreaDTO extends SysArea {
    @ApiModelProperty("行政区域子集")
    private List<SysAreaDTO> children;
}
