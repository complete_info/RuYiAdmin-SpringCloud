package com.ruyiadmin.springcloud.producer.repository.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysArea;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 行政区域 Mapper 接口
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
@Mapper
public interface ISysAreaRepository extends BaseMapper<SysArea> {

}
