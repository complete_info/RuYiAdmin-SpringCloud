package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysCodeTableDTO;
import com.ruyiadmin.springcloud.producer.domain.entity.system.SysCodeTable;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysCodeTableService extends IService<SysCodeTable> {
    /**
     * 加载数据字典缓存
     */
    void loadSysCodeTableCache();

    /**
     * 清理数据字典缓存
     */
    void clearSysCodeTableCache();

    /**
     * 获取字典树形结构
     *
     * @return QueryResult
     */
    QueryResult<SysCodeTableDTO> getCodeTreeNodes();
}
