package com.ruyiadmin.springcloud.producer.domain.dto.system;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Db Schema Entity DTO模型
 *
 * @author RuYiAdmin
 * @since 2022-08-07
 */
@Data
public class DbSchemaEntityDTO {
    private String entityName;
    private List<DbSchemaFieldDTO> fields;

    public DbSchemaEntityDTO() {
        this.setFields(new ArrayList<>());
    }

    public DbSchemaEntityDTO(String entityName) {
        this.setEntityName(entityName);
        this.setFields(new ArrayList<>());
    }

    public String toCamelLetters() {
        String[] strs = this.getEntityName().split("_");
        StringBuilder sb = new StringBuilder();
        for (String item : strs) {
            sb.append(toCamelLetters(item));
        }
        return sb.toString();
    }

    public String toCamelLetters(String str) {
        char firstChar = str.toUpperCase().charAt(0);
        String nextStr = str.toLowerCase().substring(1);
        return firstChar + nextStr;
    }
}
