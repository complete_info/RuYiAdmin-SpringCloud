package com.ruyiadmin.springcloud.producer.common.beans.system;

import com.ruyiadmin.springcloud.producer.common.factories.RuYiAdminYamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 系统计划任务配置类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-02
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "quartzconfig")
@PropertySource(value = {"classpath:appSettings.yaml"}, factory = RuYiAdminYamlPropertySourceFactory.class)
@Data
public class QuartzConfig {
    //RuYiAdminFrameworkJob调度间隔
    private String RuYiAdminFrameworkJob;
    //计划任务组
    private String ScheduleJobGroup;
    //计划任务触发器
    private String ScheduleJobTrigger;
    //是否支持集群
    private boolean SupportGroup;
    //集群编号
    private int GroupId;
    //通道名称
    private String ChanelName;
}
