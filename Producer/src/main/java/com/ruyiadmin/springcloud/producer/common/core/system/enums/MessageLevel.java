package com.ruyiadmin.springcloud.producer.common.core.system.enums;

/**
 * <p>
 * 广播消息级别
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-22
 */
public enum MessageLevel {
    //消息级别
    Information,

    //通知级别
    Notification,

    //警告级别
    Warning,

    //错误级别
    Error,

    //严重级别
    Severity,

    //紧急级别
    Emergency
}
