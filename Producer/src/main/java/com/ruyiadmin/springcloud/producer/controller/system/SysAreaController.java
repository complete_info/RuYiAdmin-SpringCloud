package com.ruyiadmin.springcloud.producer.controller.system;

import com.ruyiadmin.springcloud.producer.common.annotations.system.Log;
import com.ruyiadmin.springcloud.producer.common.annotations.system.Permission;
import com.ruyiadmin.springcloud.producer.common.core.business.enums.OperationType;
import com.ruyiadmin.springcloud.producer.common.core.system.entities.QueryResult;
import com.ruyiadmin.springcloud.producer.domain.dto.system.SysAreaDTO;
import com.ruyiadmin.springcloud.producer.service.iservices.system.ISysAreaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * <p>
 * 行政区域表 前端控制器
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-11
 */
@RestController
@RequestMapping("/AreaManagement")
@Api(tags = "系统行政区域管理服务")
@RequiredArgsConstructor
public class SysAreaController {

    //region 服务私有属性

    private final ISysAreaService areaService;

    //endregion

    //region 查询行政区域列表

    @PostMapping("/Post")
    @ApiOperation(value = "查询行政区域列表")
    @Log(OperationType = OperationType.QueryList)
    @Permission(permission = "area:query:list")
    public QueryResult<SysAreaDTO> getAreaTreeNodes() throws ExecutionException, InterruptedException {
        CompletableFuture<QueryResult<SysAreaDTO>> future =
                CompletableFuture.supplyAsync(this.areaService::getAreaTreeNodes);
        return future.get();
    }

    //endregion

}
