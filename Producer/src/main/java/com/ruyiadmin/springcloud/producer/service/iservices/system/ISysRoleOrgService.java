package com.ruyiadmin.springcloud.producer.service.iservices.system;

import com.ruyiadmin.springcloud.producer.domain.entity.system.SysRoleOrg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色机构关系表 服务类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-12
 */
public interface ISysRoleOrgService extends IService<SysRoleOrg> {

    /**
     * 加载角色与机构缓存
     */
    void loadSysRoleOrgCache();

    /**
     * 清理角色与机构缓存
     */
    void clearSysRoleOrgCache();

}
