package com.ruyiadmin.springcloud.producer.common.core.system.enums;

/**
 * <p>
 * 数据类型枚举
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-08-13
 */
public enum CellDataType {
    //小数
    DECIMAL,

    //整数
    INTEGER,

    //文本
    TEXT,

    //日期
    DATE,

    //时间
    DATETIME
}
