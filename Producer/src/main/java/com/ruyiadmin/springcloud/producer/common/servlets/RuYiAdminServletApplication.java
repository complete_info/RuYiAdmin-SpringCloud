package com.ruyiadmin.springcloud.producer.common.servlets;

import com.ruyiadmin.springcloud.producer.service.iservices.system.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * <p>
 * RuYiAdmin系统初始化类
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-14
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class RuYiAdminServletApplication {

    //region 初始化类私有属性

    private final ISysAreaService areaService;
    private final ISysCodeTableService codeTableService;
    private final ISysLanguageService languageService;
    private final ISysMenuLanguageService menuLanguageService;
    private final ISysMenuService menuService;
    private final ISysOrganizationService orgService;
    private final ISysRoleMenuService roleMenuService;
    private final ISysRoleOrgService roleOrgService;
    private final ISysRoleService roleService;
    private final ISysRoleUserService roleUserService;
    private final ISysScheduleJobService scheduleJobService;
    private final ISysUserService userService;

    //endregion

    //region PostConstruct

    @PostConstruct
    public void init() {
        //加载行政区域缓存
        this.areaService.loadSysAreaCache();
        //加载数据字典缓存
        this.codeTableService.loadSysCodeTableCache();
        //加载系统多语缓存
        this.languageService.loadSysLanguageCache();
        //加载菜单与多语缓存
        this.menuLanguageService.loadSysMenuLanguageCache();
        //加载系统菜单缓存
        this.menuService.loadSysMenuCache();
        //加载组织机构缓存
        this.orgService.loadSysOrgCache();
        //加载角色与菜单缓存
        this.roleMenuService.loadSysRoleMenuCache();
        //加载角色与机构缓存
        this.roleOrgService.loadSysRoleOrgCache();
        //加载系统角色缓存
        this.roleService.loadSysRoleCache();
        //加载角色与用户缓存
        this.roleUserService.loadSysRoleUserCache();
        //加载计划任务缓存
        this.scheduleJobService.loadBusinessScheduleJobCache();
        //加载系统用户缓存
        this.userService.loadSystemUserCache();

        //启动执行中计划任务
        this.scheduleJobService.startScheduleJob();
    }

    //endregion

    //region PreDestroy

    @PreDestroy
    public void destroy() {
        //清理行政区域缓存
        this.areaService.clearSysAreaCache();
        //清理数据字典缓存
        this.codeTableService.clearSysCodeTableCache();
        //清理系统多语缓存
        this.languageService.clearSysLanguageCache();
        //清理菜单与多语缓存
        this.menuLanguageService.clearSysMenuLanguageCache();
        //清理系统菜单缓存
        this.menuService.clearSysMenuCache();
        //清理组织机构缓存
        this.orgService.clearSysOrgCache();
        //清理角色与菜单缓存
        this.roleMenuService.clearSysRoleMenuCache();
        //清理角色与机构缓存
        this.roleOrgService.clearSysRoleOrgCache();
        //清理系统角色缓存
        this.roleService.clearSysRoleCache();
        //清理角色与用户缓存
        this.roleUserService.clearSysRoleUserCache();
        //清理计划任务缓存
        this.scheduleJobService.clearBusinessScheduleJobCache();
        //清理系统用户缓存
        this.userService.clearSystemUserCache();
    }

    //endregion

}
