package com.ruyiadmin.springcloud.producer.common.annotations.system;

import org.apache.commons.lang3.StringUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * 操作鉴权注解
 * </p>
 *
 * @author RuYiAdmin
 * @since 2022-07-20
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Permission {
    String permission() default StringUtils.EMPTY;
}
