package com.ruyiadmin.springcloud.consumer.controllers;

import com.ruyiadmin.springcloud.consumer.services.interfaces.IHelloWorldService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/helloWorld")
@RequiredArgsConstructor
public class HelloWorldController {

    private final IHelloWorldService helloWorldService;

    @RequestMapping("/sayHello")
    public String sayHello() {
        return helloWorldService.sayHello();
    }
}
