package com.ruyiadmin.springcloud.consumer.services.interfaces;

import com.ruyiadmin.springcloud.consumer.services.impls.HelloWorldServiceHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "PRODUCER", fallback = HelloWorldServiceHystrix.class)
public interface IHelloWorldService {
    @RequestMapping(value = "/API/helloWorld/sayHello", method = RequestMethod.GET)
    String sayHello();
}
