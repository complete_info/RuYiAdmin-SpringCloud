package com.ruyiadmin.springcloud.consumer.services.impls;

import com.ruyiadmin.springcloud.consumer.services.interfaces.IHelloWorldService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

//@Service
//@RequiredArgsConstructor
//public class HelloWorldService implements IHelloWorldService {
//    private final RestTemplate restTemplate;
//
//    public String sayHello() {
//        //服务地址 http://{服务提供者应用名名称}/{具体的controller}
//        String url = "http://DEMO-PRODUCER/producer/sayHello";
//        //返回值类型和我们的业务返回值一致
//        return restTemplate.getForObject(url, String.class);
//    }
//}
