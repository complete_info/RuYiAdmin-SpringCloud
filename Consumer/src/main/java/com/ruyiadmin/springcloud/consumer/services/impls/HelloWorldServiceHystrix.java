package com.ruyiadmin.springcloud.consumer.services.impls;

import com.ruyiadmin.springcloud.consumer.services.interfaces.IHelloWorldService;
import org.springframework.stereotype.Component;

@Component
public class HelloWorldServiceHystrix implements IHelloWorldService {

    @Override
    public String sayHello() {
        return "请求失败！！";
    }

}
