# RuYiAdmin-SpringCloud

#### 项目介绍
```
RuYiAdmin-SpringCloud是如意Admin家族的Java SpringCloud版本。

RuYiAdmin-SpringCloud是一款前后端分离的、代码能够自动生成的、基于JDK11、
SpringBoot2.7.4、SpringCloud2021.0.4的、可以跨平台的、低代码、分布式、
多线程、高性能企业级RBAC Web后台管理解决方案。前端采用最受欢迎的开源框架之一：
Vue Element Admin，后端采用服务与仓储模式。支持系统集成与统一认证。

系统业务支持智慧大屏幕。可一键生成多个业务的视图层、控制层、服务层、Mapper层、领
域层代码。集成VForm表单设计器，支持PC、Pad、手机H5页面表单设计与预览。ORM采用阿
里巴巴MyBatisPlus。数据库连接池采用阿里巴巴德鲁伊，支持一主两从、读写分离，支持
数据库审计。支持SpringBootAdmin，支持多应用管理。关系库支持MySql数据库，非关系
库支持Redis、MongoDB、Elasticsearch（Meilisearch后续适配）。分布式消息总线
支持ActiveMQ、Redis。企业级配置中心支持阿里巴巴Nacos。接口文档使用Knife4j。支
持统一异常处理。支持分布式任务的管理与调度。项目防Token劫持，支持国产SM加密算法。
支持ConfigServer远程配置，支持Eureka服务注册与发现，支持gateway网关和负载均衡，
支持支持OpenFeign和Hystrix熔断降级，支持Zipkin链路追踪。支持数据库Swagger：
screw。支持Sentinel限流、熔断和降级，支持限流规则持久化至Nacos。

企业版高端交流群：806522770。商用与技术咨询VIP群：664802523。
```

.NET7或者SqlServer、SQLite、Oracle、PostgreSQL、华为OpenGauss数据库结构请移步：https://gitee.com/pang-mingjun/RuYiAdmin

系统前端和mysql数据库请移步SpringBoot版本：https://gitee.com/pang-mingjun/RuYiAdmin-SpringBoot

#### 郑重声明

```
版权所有、未经授权、禁止商用、侵权必究

由于如意Admin的.NET版本被大量抄袭、模仿，甚至被用于商业用途，为保证项目的可
持续发展、构建良好的开源氛围，因此如意Admin开源项目的协议由Apache2.0变更为
AGPL3.0。协议变更以后，未经授权，任何人不得使用本项目代码用于商业用途，否则需
要承担法律责任。个人和开源意图的使用不受限制（不包含个人商用的情况）。

如意Admin项目的开源意图是“授人以渔”，并非让使用者借机商用。对于未经授权的商用
，可以通过QQ群进行举报，对于有效举报将进行奖励（额度为维权成功费用的5%）。

对于不尊重知识版本、肆意剽窃、代码相似程度大于50%的项目也可以进行举报（奖励额
度为维权成功费用的5%）。
```

#### 安装教程

1. 安装JDK11、配置环境变量、生成jre。
1. 安装Idea。有条件的粉丝建议使用企业版Idea，前后端的开发工作可以在一个工具里同时进行。
1. 配置maven。
1. 安装并启用Nacos。
1. 安装并启用Zipkin。
1. 安装Redis。
1. 安装ActiveMQ。
1. 安装Mysql数据库，支持一主两从、读写分离。作者使用的是mysql8。
1. 安装非关系库。审计日志默认进入非关系库MongoDB，请安装MongoDB。使用Elasticsearch请启用ES服务。
1. 安装NodeJs。
1. 克隆代码。
1. 初始化数据库。结构与数据位于sql文件夹下。如果使用低版本mysql，可能需要修改数据库初始化脚本。
   使用SqlServer、SQLite、Oracle、PostgreSQL和华为OpenGauss数据库需要修改用户密码（用户的初始密码均为123456）。修改原因是C#使用的加密算法是AES/CBC/PKCS7，而java使用的加密算法是AES/CBC/PKCS5PAdding。涉及表为sys_user和biz_user，涉及字段为password和salt。可以将这两个字段的值修改为mysql版本任一用户的密码及其salt（比如password：ihKjJgmA1ZcHqeLPOlyDkxGcKrowBD8mxqCIotNSFkar95A16Cd3bFMaftKLGXRT，salt：a8222bd4-c246-4778-9c5e-bc988dbd85bf）。此外，后端需要添加相应数据库驱动，修改数据库连接串配置、修改MyBatisPlus配置。
1. 修改配置。修改后端application.properties中Redis、ActiveMQ、Mysql、MongoDB数据库连接串。前端mq配置位于src/constants/active-mq.js。
1. 系统前端。前端管理目录位于vue。推荐使用Visual Studio Code开源软件编辑。前端的使用可以参看Vue Element Admin官网。
1. 服务启动顺序：Eureka>SpringBootAdmin>Producer>Gateway>Consumer>ConfigServer>ConfigClient。

#### 系统截图

1. 系统登录（默认账号：admin，密码：123456）
   ![输入图片说明](images/dl1.png)
   ![输入图片说明](images/denglu.png)
1. 系统首页
   ![输入图片说明](images/sy.png)
1. 机构管理
   ![输入图片说明](images/jggl.png)
1. 用户管理
   ![输入图片说明](images/yhgl.png)
1. 菜单管理
   ![输入图片说明](images/cdgl.png)
1. 角色管理
   ![输入图片说明](images/jsgl.png)
1. 数据字典
   ![输入图片说明](images/sjzd.png)
1. 导入配置
   ![输入图片说明](images/drpz.png)
   ![输入图片说明](images/drpzmx.png)
1. 任务调度
   ![输入图片说明](images/rwdd.png)
1. 多语管理
   ![输入图片说明](images/dygl.png)
1. 行政区域管理
   ![输入图片说明](images/xzqy.png)
1. 通知公告管理
   ![输入图片说明](images/tzgg.png)

1. 审计日志监控
   ![输入图片说明](images/sjrz.png)
   ![输入图片说明](images/sjrzmx.png)
1. 在线用户监控
   ![输入图片说明](images/zxyhjk.png)
1. 服务器监控
   ![输入图片说明](images/fwqjk.png)
1. 系统文件监控
   ![输入图片说明](images/xtwjjk.png)
1. 数据库审计
   ![输入图片说明](images/sjksj.png)
1. 系统应用监控
   ![输入图片说明](images/xtyyjk.png)

1. 系统集成-模块管理
   ![输入图片说明](images/mokuaiguanli.png)
1. 系统集成-统一授权管理
   ![输入图片说明](images/shouquanguanli.png)
1. 系统集成-离态用户管理
   ![输入图片说明](images/litaiyonghuguanli.png)
1. 系统集成-同步账号管理
   ![输入图片说明](images/tongbuzhanghaoguanli.png)
1. 统一认证-统一登录
   ![输入图片说明](images/tongyirenzh.png)
1. 统一认证-统一访问
   ![输入图片说明](images/tongyifangwen.png)

1. 系统公告
   ![输入图片说明](images/xtgg.png)
1. 系统通知
   ![输入图片说明](images/xttz.png)

1. 系统手册
   ![输入图片说明](images/xtsc.png)

1. 表单设计器
   ![输入图片说明](images/bdsjq.png)
1. 代码生成器
   ![输入图片说明](images/dmscq.png)
   ![输入图片说明](images/scdm.png)
1. 接口说明文档
   ![输入图片说明](images/1663919799390.jpg)

1. 智慧大屏幕
   ![输入图片说明](images/zhdpm.png)

1. 常规数据导入校验
   ![输入图片说明](images/drjy1.png)
   ![输入图片说明](images/drjy2.png)
